/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientEngine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine {
  import engine.common.core.Engine;
  import engine.common.core.EngineInitData;
  import engine.common.data.MessageProperties;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.PlayerStatuses;
  import engine.common.data.ServerMessageTypes;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.ActiveGameChangedKeys;
  import engine.common.data.messageKeys.GameStateChangedKeys;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.RequestComponentThemeImageDataKeys;
  import engine.common.data.messageKeys.RoundTimerUpdateKeys;
  import engine.common.data.messageKeys.RoundWinnerKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.gameManager.CnGameFactory;
  import engine.games.CnGame;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.lobbyView.CnLobbyView;
  import engine.ui.mainView.CnMainView;
  import engine.ui.mainView.I_CnMainViewCallbacks;
  import engine.utils.themeImageFileWriter.CnComponentThemeImageFileWriter;
  import engine.utils.themeImageFileWriter.I_CnComponentThemeImageFileWriterCallbacks;
  import flib.utils.FDataBlock;
  import flib.utils.framesDelayer.IFFramesDelayerCallbacks;
  import net.data.NetClientData;
  
  public class ClientEngine extends Engine implements I_ClientEngine, I_CnMainViewCallbacks,
  IFFramesDelayerCallbacks, I_CnComponentThemeImageFileWriterCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: see RoundWinnerKeys for data format
    private var mao_winningPlayers:Array;
    
    private var mo_game:CnGame;
    private var mo_componentThemeImageFileWriter:CnComponentThemeImageFileWriter;
    private var mb_isWaitingForNextGame:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ClientEngine(o_initData:EngineInitData):void {
      super(o_initData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // activeGameId (get)
    //==================================================================
    public function get activeGameId():String {
      return(serverState.getString(ServerStateKeys.ACTIVE_GAME_ID));
    }
    
    //==================================================================
    // frameDelayerOnComplete
    //==================================================================
    public function frameDelayerOnComplete(u_id:uint, o_data:Object):void {
      var s_actveGameId:String = activeGameId;
      mo_game = CnGameFactory.Create(this, s_actveGameId);
    }
    
    //==================================================================
    // gameOnThemeImagesComplete
    //==================================================================
    public function gameOnThemeImagesComplete():void {
      hideLobbyView();
    }
    
    //==================================================================
    // isWaitingForNextGame (get)
    //==================================================================
    public function get isWaitingForNextGame():Boolean {
      return(mb_isWaitingForNextGame);
    }
    
    //==================================================================
    // onClientIsLeaving
    //==================================================================
    public override function onClientIsLeaving(s_clientId:String, b_isBecomingObserver:Boolean):void {
      //This method is designed to be called by the ClientLeftHandler.
      
      var o_playerData:FDataBlock = getPlayerData(s_clientId);
      if (o_playerData.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.OBSERVER) {
        return; //this function n/a for observers
      }
    }
    
    //==================================================================
    // onReadyToStartGame
    //==================================================================
    public function onReadyToStartGame():void {
      uninitIntroView();
    }
    
    //==================================================================
    // themeImageFileWriteOnComplete
    //==================================================================
    public function themeImageFileWriteOnComplete(o_writer:CnComponentThemeImageFileWriter):void {
      loadThemeComponentImages(false);
    }
    
    //==================================================================
    // winningPlayers (get)
    //==================================================================
    public function get winningPlayers():Array {
      return(mao_winningPlayers);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // allObserverClientsBecomeActive
    //==================================================================
    private function allObserverClientsBecomeActive():void {
      var o_players:FDataBlock = serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      var ao_playerStates:Array = o_players.getValues();
      for each (var o_playerState:FDataBlock in ao_playerStates) {
        if (o_playerState.getBoolean(PlayerStateKeys.STATUS) == PlayerStatuses.OBSERVER) {
          o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.ACTIVE);
        }
      }
    }
    
    //==================================================================
    // onHandleServerState
    //==================================================================
    override protected function onHandleServerState(b_activeGameRequestFromInitialState:Boolean,
    b_activeGameStartedRequestFromInitialState:Boolean):void {
      handleServerStateSetupClients(b_activeGameRequestFromInitialState,
        b_activeGameStartedRequestFromInitialState);
    }
    
    //==================================================================
    // onHandleSocketMessage
    //==================================================================
    protected override function onHandleSocketMessage(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      //handles messages sent from the server
      
      super.onHandleSocketMessage(o_clientData, o_messageBlock);
      
      var s_type:String = o_messageBlock.getString(MessageProperties.TYPE);
      switch (s_type) {
        case ServerMessageTypes.ACTIVE_GAME_ID_CHANGED:
          handleMessageActiveGameIdChanged(o_clientData, o_messageBlock);
          break;
          
        case ServerMessageTypes.CLIENT_JOINED:
          handleMessageClientJoinedCl(o_clientData, o_messageBlock);
          break;
          
        case ServerMessageTypes.COMPONENT_THEME_IMAGES:
          handleMessageComponentThemeImages(o_clientData, o_messageBlock);
          break;
          
        case ServerMessageTypes.GAME_STATE_CHANGED:
          handleMessageGameStateChanged(o_clientData, o_messageBlock);
          break;
        
        case ServerMessageTypes.ROUND_TIMER_UPDATE:
          handleMessageRoundTimerUpdate(o_clientData, o_messageBlock);
          break;
        
        case ServerMessageTypes.ROUND_WINNER:
          handleMessageRoundWinner(o_clientData, o_messageBlock);
          break;
      }
    }
    
    //==================================================================
    // onInitGame
    //==================================================================
    override protected function onInitGame():void {
      initGame();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getCnMainView
    //==================================================================
    private function getCnMainView():CnMainView {
      return(mainView as CnMainView);
    }
    
    //==================================================================
    // getClientName
    //==================================================================
    private function getClientName(s_clientId:String):String {
      var o_playerData:FDataBlock = getPlayerData(s_clientId);
      if (!o_playerData) {
        return(null);
      }
      
      var o_userData:UserData = o_playerData.getObject(PlayerStateKeys.USER_DATA) as UserData;
      return(o_userData ? o_userData.name : null);
    }
    
    //==================================================================
    // getClientNamesOf
    //==================================================================
    private function getClientNamesOf(as_clientIds:Array):Array {
      var as_playerNames:Array = [];
      
      for each (var s_clientId:String in as_clientIds) {
        var s_name:String = getClientName(s_clientId);
        if (s_name) {
          as_playerNames.push(s_name);
        }
      }
      
      return(as_playerNames);
    }
    
    //==================================================================
    // handleActiveGameChanged
    //==================================================================
    private function handleActiveGameChanged():void {
      allObserverClientsBecomeActive();
      
      var s_activeGameId:String = serverState.getString(ServerStateKeys.ACTIVE_GAME_ID);
      if (s_activeGameId) {
        updateLobbyViewForSelectedGame();
        loadThemeComponentImages(true);
      } else {
        mb_isWaitingForNextGame = false;
        uninitGame();
        resetPlayerData();
        updateLobbyViewForNoGame();
      }
    }
    
    //==================================================================
    // handleGameStarted
    //==================================================================
    private function handleGameStarted():void {
      if (mo_game) {
        mo_game.hideWaitingDisplay();
        mo_game.showContentDisplay();
        mo_game.updateEntry();
      }
    }
    
    //==================================================================
    // handleGameStopped
    //==================================================================
    private function handleGameStopped():void {
      uninitGame();
      handleActiveGameChanged();
      resetPlayerData();
    }
    
    //==================================================================
    // handleMessageActiveGameIdChanged
    //==================================================================
    private function handleMessageActiveGameIdChanged(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      var s_activeGameId:String = o_messageBlock.getString(ActiveGameChangedKeys.GAME_ID);
      if (s_activeGameId == activeGameId) {
        return;
      }
      
      serverState.setProperty(ServerStateKeys.ACTIVE_GAME_ID, s_activeGameId);
      
      var o_gameSettings:Object = o_messageBlock.getObject(ActiveGameChangedKeys.GAME_SETTINGS);
      serverState.setProperty(ServerStateKeys.ACTIVE_GAME_SETTINGS, o_gameSettings);
      
      handleActiveGameChanged();
    }
    
    //==================================================================
    //handleMessageClientJoinedCl
    //==================================================================
    private function handleMessageClientJoinedCl(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      //to-do
    }
    
    //==================================================================
    // handleMessageComponentThemeImages
    //==================================================================
    private function handleMessageComponentThemeImages(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      trace("ClientEngine.handleMessageComponentThemeImages. " + o_clientData.userData.id);
      writeComponentThemeImageFiles(o_messageBlock.getObject(RequestComponentThemeImageDataKeys.REQUESTS));
    }
    
    //==================================================================
    // handleMessageRoundWinner
    //==================================================================
    private function handleMessageRoundWinner(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      if (mo_game) {
        mao_winningPlayers = o_messageBlock.getArray(RoundWinnerKeys.WINNING_PLAYERS);
        mo_game.showResultsDisplay(mao_winningPlayers);
      }
    }
    
    //==================================================================
    // handleMessageGameStateChanged
    //==================================================================
    private function handleMessageGameStateChanged(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      var b_isGameStarted:Boolean = o_messageBlock.getBoolean(GameStateChangedKeys.IS_STARTED);
      serverState.setProperty(ServerStateKeys.IS_GAME_STARTED, b_isGameStarted);
      
      mao_winningPlayers = null;
      
      if (b_isGameStarted) {
        handleGameStarted();
      } else {
        handleGameStopped();
      }
    }
    
    //==================================================================
    // handleMessageRoundTimerUpdate
    //==================================================================
    private function handleMessageRoundTimerUpdate(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      if (mb_isWaitingForNextGame) {
        return;
      }
      
      if (!mo_game) {
        return;
      }
      
      var i_elapsedSeconds:int = o_messageBlock.getNumber(RoundTimerUpdateKeys.ELAPSED);
      var i_maxSeconds:int = o_messageBlock.getNumber(RoundTimerUpdateKeys.MAX);
      mo_game.onRoundTimerUpdate(i_elapsedSeconds, i_maxSeconds);
    }
    
    //==================================================================
    // handleServerStateSetupClients
    //==================================================================
    private function handleServerStateSetupClients(b_activeGameRequestFromInitialState:Boolean,
    b_activeGameStartedRequestFromInitialState:Boolean):void {
      //only care about this player when setting up states - other players are not presented to this player
      var o_playerData:FDataBlock = getPlayerData(playerId);
      if (!o_playerData) {
        return;
      }
      
      if (!b_activeGameRequestFromInitialState) {
        handleActiveGameChanged();
      }
      
      var b_isGameStarted:Boolean = serverState.getBoolean(ServerStateKeys.IS_GAME_STARTED);
      if (b_isGameStarted) {
        waitForNextGame();
      } else {
        handleGameStopped();
      }
    }
    
    //==================================================================
    // hideLobbyView
    //==================================================================
    private function hideLobbyView():void {
      var o_mainView:CnMainView = getCnMainView();
      if (o_mainView) {
        o_mainView.lobbyView.visible = false;
      }
    }
    
    //==================================================================
    // initGame
    //==================================================================
    private function initGame():void {
      uninitGame();
      
      var s_actveGameId:String = activeGameId;
      mo_game = CnGameFactory.Create(this, s_actveGameId);
      if (mo_game) {
        mo_game.showWaitingDisplay(CnBaseGameView.WAITING_MODE_NEXT_BUTTON);
      } else {
        trace("ClientEngine.initGame. Warning: No valid game created.");
      }
    }
    
    //==================================================================
    // resetPlayerData
    //==================================================================
    private function resetPlayerData():void {
      var o_playerData:FDataBlock = thisPlayerData;
      var o_progress:PlayerGameProgress =
        o_playerData.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress;
      o_progress.reset();
    }
    
    //==================================================================
    // uninitGame
    //==================================================================
    private function uninitGame():void {
      if (mo_game) {
        mo_game.uninit();
        mo_game = null;
      }
    }
    
    //==================================================================
    // updateGameEntry
    //==================================================================
    private function updateGameEntry():void {
      if (mo_game) {
        mo_game.updateEntry();
      } else {
        trace("ClientEngine.updateGameEntry. Warning: No game object set.");
      }
    }
    
    //==================================================================
    // updateLobbyViewForNoGame
    //==================================================================
    private function updateLobbyViewForNoGame():void {
      var o_lobbyView:CnLobbyView = getCnMainView().lobbyView;
      o_lobbyView.visible = true;
      o_lobbyView.noSelectedGameText();
    }
    
    //==================================================================
    // updateLobbyViewForSelectedGame
    //==================================================================
    private function updateLobbyViewForSelectedGame():void {
      var o_lobbyView:CnLobbyView = getCnMainView().lobbyView;
      o_lobbyView.visible = true;
      o_lobbyView.selectedGameText = activeGameId;
    }
    
    //==================================================================
    // waitForNextGame
    //==================================================================
    private function waitForNextGame():void {
      mb_isWaitingForNextGame = true;
      mo_game.showWaitingDisplay(CnBaseGameView.WAITING_MODE_NEXT_GAME_LABEL);
      isBusyWithMessage = false;
      processNextMessage();
    }
    
    //==================================================================
    // writeComponentThemeImageFiles
    //==================================================================
    private function writeComponentThemeImageFiles(o_imageDataByKey:Object):void {
      if (!mo_componentThemeImageFileWriter) {
        mo_componentThemeImageFileWriter =
          new CnComponentThemeImageFileWriter(app.deviceProfile.id, this);
      }
      
      mo_componentThemeImageFileWriter.run(o_imageDataByKey);
    }
  }
}