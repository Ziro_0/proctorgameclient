/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnDesignData

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-06-2017
  ActionScript:	3.0
  Description:	
  History:
    04-06-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data {

  public final class CnDesignData {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const THEMES_BASE_URL:String = "themes/";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}