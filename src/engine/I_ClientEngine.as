/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  IClientEngine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine {
  import engine.common.core.I_Engine;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_ClientEngine extends I_Engine {
    function get activeGameId():String;
    function gameOnThemeImagesComplete():void;
    function get isWaitingForNextGame():Boolean;
    function onReadyToStartGame():void;
    function get winningPlayers():Array;
  }
}