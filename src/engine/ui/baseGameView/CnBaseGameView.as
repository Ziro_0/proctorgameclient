/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnBaseGameView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-08-2018
  ActionScript:  3.0
  Description:  
  History:
    07-08-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.baseGameView {

  import engine.I_ClientEngine;
  import engine.common.baseView.BaseView;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.simple.SimpleTextView;
  import engine.common.ui.softKeyboardTextInputHandler.I_SoftKeyboardTextInputHandlerCallbacks;
  import engine.common.ui.softKeyboardTextInputHandler.SoftKeyboardTextInputHandler;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.data.CnDebugData;
  import engine.ui.themeImagesHelper.CnThemeImagesHelper;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flib.utils.FDataBlock;
  import flib.utils.pckgLvl.fPadDigits;
	
  public class CnBaseGameView extends BaseView implements I_InputLabelCallbacks,
  I_SoftKeyboardTextInputHandlerCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const GAME_STARTED_BACK_NODE_NAME:String = "game_started_back_node";
    public static const GAME_SELECTED_BACK_NODE_NAME:String = "game_selected_back_node";
    public static const RESULTS_BACK_NODE_NAME:String = "results_back_node";
    public static const PANEL_NODE_NAME:String = "panel_node";
    public static const BANNER_NODE_NAME:String = "banner_node";
    
    public static const WAITING_MODE_NEXT_BUTTON:uint = 0;
    public static const WAITING_MODE_READY_BUTTON:uint = 1;
    public static const WAITING_MODE_START_LABEL:uint = 2;
    public static const WAITING_MODE_NEXT_GAME_LABEL:uint = 3;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_ClientEngine;
    private var mo_resultsView:CnResultsView;
    private var mo_waitingView:CnWaitingView;
    private var mo_timerView:SimpleTextView;
    private var mo_nextButton:Button;
    private var mo_backButton:Button;
    private var mo_finishButton:Button;
    private var mo_primaryInputLabel:InputLabel;
    private var mo_contentDisplay:Sprite;
    
    //key: String (view display mode)
    //data: DisplayObject
    private var mo_displayModeViews:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnBaseGameView(o_spriteOrSpriteClass:Object, o_engine:I_ClientEngine,
    o_callbacks:I_CnBaseGameViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
			init(o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enabled (set)
    //==================================================================
    override public function set enabled(b_value:Boolean):void {
      if (!b_value) {
        display.stage.focus = null;
      }
      
      super.enabled = b_value;
    }
    
    //==================================================================
    // getEngine
    //==================================================================
    public function getEngine():I_ClientEngine {
      return(mo_engine);
    }
    
    //==================================================================
    // hideAllDisplayModes
    //==================================================================
    public function hideAllDisplayModes():void {
      showDisplayMode(null);
    }
    
    //==================================================================
    // hideWaitingDisplay
    //==================================================================
    public function hideWaitingDisplay():void {
      mo_waitingView.visible = false;
    }
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
    }
    
    //==================================================================
    // inputTextHandlerOnSoftKybdChange
    //==================================================================
    public function inputTextHandlerOnSoftKybdChange(o_handler:SoftKeyboardTextInputHandler,
    n_offset:Number):void {
    }
    
    //==================================================================
    // inputTextHandlerOnSoftKybdComplete
    //==================================================================
    public function inputTextHandlerOnSoftKybdComplete(o_handler:SoftKeyboardTextInputHandler):void {
      next();
    }
    
    //==================================================================
    // nextButtonsEnabled (set)
    //==================================================================
    public function set nextButtonsEnabled(b_value:Boolean):void {
      enableButton(mo_finishButton, b_value);
      enableButton(mo_nextButton, b_value);
    }
    
    //==================================================================
    // primaryInputText (set)
    //==================================================================
    public function set primaryInputText(s_text:String):void {
      if (mo_primaryInputLabel) {
        mo_primaryInputLabel.text = s_text;
      }
    }
    
    //==================================================================
    // response (get)
    //==================================================================
    public function get response():Object {
      if (mo_primaryInputLabel) {
        return(mo_primaryInputLabel.text);
      }
      
      trace("CnBaseGameView.response. Warning: Unless a primary input label is being used, this base " +
        "method should be overridden to provide the apprppriate response.");
      return(null);
    }
    
    //==================================================================
    // showContentDisplay
    //==================================================================
    public function showContentDisplay():void {
      showDisplayMode(CnBaseGameViewDisplayModes.CONTENT);
      hideGameModelView();
    }
    
    //==================================================================
    // showFinishButton
    //==================================================================
    public function showFinishButton():void {
      BaseView.ShowDisplay(mo_nextButton, false);
      BaseView.ShowDisplay(mo_finishButton, true);
    }
    
    //==================================================================
    // showResultsBackView
    //==================================================================
    private function showResultsBackView():void {
      BaseView.ShowDisplay(getSprite(RESULTS_BACK_NODE_NAME), true);
      BaseView.ShowDisplay(getSprite(GAME_STARTED_BACK_NODE_NAME), false);
      BaseView.ShowDisplay(getSprite(GAME_SELECTED_BACK_NODE_NAME), false);
    }
    
    //==================================================================
    // showResultsDisplay
    //==================================================================
    public function showResultsDisplay(s_header:String, ao_winningPlayers:Array,
    b_showNonWinnerHeader:Boolean):void {
      showDisplayMode(CnBaseGameViewDisplayModes.RESULTS);
      BaseView.ShowDisplay(getSprite(PANEL_NODE_NAME), false);
      showResultsBackView();
      
      mo_resultsView.showResultsHeader();
      mo_resultsView.winningPlayers = ao_winningPlayers;
    }
    
    //==================================================================
    // showWaitingDisplay
    //==================================================================
    public function showWaitingDisplay(u_waitingMode:uint):void {
      mo_waitingView.visible = true;
      
      switch (u_waitingMode) {
        case CnBaseGameView.WAITING_MODE_START_LABEL:
          mo_waitingView.showWaitingToStartLabel();
          break;
          
        case CnBaseGameView.WAITING_MODE_NEXT_BUTTON:
          mo_waitingView.showNextButton();
          showGameModelView();
          break;
          
        case CnBaseGameView.WAITING_MODE_READY_BUTTON:
          mo_waitingView.showReadyButton();
          break;
          
        case CnBaseGameView.WAITING_MODE_NEXT_GAME_LABEL:
          mo_waitingView.showWaitingToNextGameLabel();
          break;
      }
    }
    
    //==================================================================
    // themeImagesHelperOnComplete
    //==================================================================
    public function themeImagesHelperOnComplete():void {
      var o_callbacks:I_CnBaseGameViewCallbacks = callbacks as I_CnBaseGameViewCallbacks;
      if (o_callbacks) {
        o_callbacks.gameViewOnThemeImagesComplete(this);
      }
    }
    
    //==================================================================
    // timerValue (set)
    //==================================================================
    /**
     * Sets the time that the timer UI will display.
     * @param n_uValue - The time, in seconds, that will be shown. The value is converted to this format: m:ss
     */
    public function set timerValue(n_uValue:Number):void {
      if (isNaN(n_uValue) || n_uValue < 0) {
        return;
      }
      
      n_uValue = Math.floor(n_uValue); //remove any fraaction
      var n_minutes:Number = Math.floor(n_uValue / 60);
      var n_seconds:Number = Math.floor(n_uValue % 60);
      mo_timerView.value = n_minutes.toString() + ":" + fPadDigits(n_seconds, 2);
    }
    
    //==================================================================
    // timerView (get)
    //==================================================================
    public function get timerView():SimpleTextView {
      return(mo_timerView);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_nextButton = uninitButton(mo_nextButton);
      mo_backButton = uninitButton(mo_backButton);
      mo_finishButton = uninitButton(mo_finishButton);
      uninitPrimaryInputLabel();
      mo_waitingView.uninit();
      mo_engine = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableButton
    //==================================================================
    protected function enableButton(o_button:Button, b_value:Boolean):void {
      if (o_button) {
        o_button.enabled = b_value;
      }
    }
    
    //==================================================================
    // hideButtons
    //==================================================================
    protected function hideButtons():void {
      BaseView.ShowDisplay([ mo_nextButton, mo_backButton, mo_finishButton], false);
    }
    
    //==================================================================
    // init
    //==================================================================
    protected function init(o_engine:I_ClientEngine):void {
      mo_engine = o_engine;
      showDisabledFilter = false;
      
      mo_displayModeViews = { };
      
      initContentDisplay();
      initResultsView();
      initWaitingDisplay();
      
      onInitContentDisplay(mo_contentDisplay);
      
      initThemeImages();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    protected function initButtons():void {
      const CLICK_SOUND:Object = false;
      const ENABLED:Boolean = true;
      
      mo_nextButton = initButton("next_button_display", onNextButton, CLICK_SOUND, ENABLED,
        mo_contentDisplay);
        
      mo_backButton = initButton("back_button_display", onBackButton, CLICK_SOUND, ENABLED,
        mo_contentDisplay);
        
      mo_finishButton = initButton("finish_button_display", onFinishButton, CLICK_SOUND, ENABLED,
        mo_contentDisplay);
    }
    
    //==================================================================
    // initPrimaryInputLabel
    //==================================================================
    protected function initPrimaryInputLabel(s_textfieldName:String,
    s_defaultText:String = null):InputLabel {
      if (!mo_primaryInputLabel) {
        const INSTRUCTIONS_NAME_OR_FIELD:Object = null;
        mo_primaryInputLabel = new InputLabel(mo_contentDisplay, this, s_defaultText,
          s_textfieldName, INSTRUCTIONS_NAME_OR_FIELD);
        mo_primaryInputLabel.initSoftKeyboardHandler(getEngine(), mo_contentDisplay, this);
        mo_primaryInputLabel.debugDrawSoftKeyboardShapesEnabled = CnDebugData.SHOW_SOFT_KYBD_SHAPES;
      }
      return(mo_primaryInputLabel);
    }
    
    //==================================================================
    // initThemeImages
    //==================================================================
    protected function initThemeImages():CnThemeImagesHelper {
      var o_themeImagesHelper:CnThemeImagesHelper = new CnThemeImagesHelper(this, getEngine());
      var o_gameSettings:FDataBlock =
        mo_engine.serverState.createBlock(ServerStateKeys.ACTIVE_GAME_SETTINGS);
      var s_gameModel:String = o_gameSettings.getString("model");
      o_themeImagesHelper.addThemeComponentsImages(s_gameModel);
      return(o_themeImagesHelper);
    }
    
    //==================================================================
    // onInitContentDisplay
    //==================================================================
    protected function onInitContentDisplay(o_contentContainer:DisplayObjectContainer):void {
    }
    
    //==================================================================
    // uninitPrimaryInputLabel
    //==================================================================
    protected function uninitPrimaryInputLabel():void {
      if (mo_primaryInputLabel) {
        mo_primaryInputLabel.uninit();
        mo_primaryInputLabel = null;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // hideGameModelView
    //==================================================================
    private function hideGameModelView():void {
      BaseView.ShowDisplay(getSprite(GAME_STARTED_BACK_NODE_NAME), true);
      BaseView.ShowDisplay(getSprite(GAME_SELECTED_BACK_NODE_NAME), false);
    }
    
    //==================================================================
    // hidePrimaryInputText
    //==================================================================
    private function hidePrimaryInputText():void {
      BaseView.ShowDisplay(mo_primaryInputLabel, false);
    }
    
    //==================================================================
    // initContentDisplay
    //==================================================================
    private function initContentDisplay():void {
      mo_contentDisplay = getSprite("content_display");
      
      if (!mo_contentDisplay) {
        trace("CnBaseGameView.initContentDisplay. Warning: missing sprite 'content_display'.");
      }
      
      mo_displayModeViews[CnBaseGameViewDisplayModes.CONTENT] = mo_contentDisplay;
      initButtons();
      initTimerView();
    }
    
    //==================================================================
    // initResultsView
    //==================================================================
    private function initResultsView():void {
      mo_resultsView = new CnResultsView(getSprite("results_display"), mo_engine);
      mo_resultsView.visible = false;
      mo_displayModeViews[CnBaseGameViewDisplayModes.RESULTS] = mo_resultsView;
    }
    
    //==================================================================
    // initTimerView
    //==================================================================
    private function initTimerView():void {
      mo_timerView = new SimpleTextView(getSprite("timer_view_display", mo_contentDisplay));
      mo_timerView.visible = false;
    }
    
    //==================================================================
    // initWaitingDisplay
    //==================================================================
    private function initWaitingDisplay():void {
      mo_waitingView = new CnWaitingView(getSprite("waiting_to_start_game_display"),
        waitingViewOnNextButtonPressed, waitingViewOnReadyButtonPressed);
    }
    
    //==================================================================
    // next
    //==================================================================
    private function next():void {
      var o_callbacks:I_CnBaseGameViewCallbacks = callbacks as I_CnBaseGameViewCallbacks;
      if (o_callbacks) {
        o_callbacks.gameViewOnNext(this);
      }
    }
    
    //==================================================================
    // onBackButton
    //==================================================================
    private function onBackButton(b_shift:Boolean):void {
      var o_callbacks:I_CnBaseGameViewCallbacks = callbacks as I_CnBaseGameViewCallbacks;
      if (o_callbacks) {
        o_callbacks.gameViewOnBack(this);
      }
    }
    
    //==================================================================
    // onFinishButton
    //==================================================================
    private function onFinishButton(b_shift:Boolean):void {
      next();
    }
    
    //==================================================================
    // onNextButton
    //==================================================================
    private function onNextButton(b_shift:Boolean):void {
      next();
    }
    
    //==================================================================
    // showDisplayMode
    //==================================================================
    private function showDisplayMode(s_displayMode:String):void {
      for (var s_displayModeLoop:String in mo_displayModeViews) {
        var o_display:Object = mo_displayModeViews[s_displayModeLoop];
        BaseView.ShowDisplay(o_display, s_displayMode == s_displayModeLoop);
      }
    }
    
    //==================================================================
    // showGameModelView
    //==================================================================
    private function showGameModelView():void {
      BaseView.ShowDisplay(getSprite(GAME_STARTED_BACK_NODE_NAME), false);
      BaseView.ShowDisplay(getSprite(GAME_SELECTED_BACK_NODE_NAME), true);
    }
    
    //==================================================================
    // waitingViewOnNextButtonPressed
    //==================================================================
    private function waitingViewOnNextButtonPressed():void {
      var o_callbacks:I_CnBaseGameViewCallbacks = callbacks as I_CnBaseGameViewCallbacks;
      if (o_callbacks) {
        o_callbacks.gameViewOnWaitingViewNext(this);
      }
    }
    
    //==================================================================
    // waitingViewOnReadyButtonPressed
    //==================================================================
    private function waitingViewOnReadyButtonPressed():void {
      var o_callbacks:I_CnBaseGameViewCallbacks = callbacks as I_CnBaseGameViewCallbacks;
      if (o_callbacks) {
        o_callbacks.gameViewOnWaitingViewReady(this);
      }
    }
  }
}