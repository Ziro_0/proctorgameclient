/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnWaitingView

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          06-21-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.baseGameView {
  import engine.common.baseView.BaseView;
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.ui.buttons.Button;
  import flash.events.Event;
  import flash.text.TextField;
	
  public class CnWaitingView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_nextButton:Button;
    private var mo_readyButton:Button;
    private var mo_txfWaitingToStart:TextField;
    private var mo_txfWaitingToNextGame:TextField;
    
    //takes no params
    private var mf_onReadyButtonPressed:Function;
    private var mf_onNextButtonPressed:Function;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnWaitingView(o_spriteOrSpriteClass:Object, f_onNextButtonPressed:Function,
    f_onReadyButtonPressed:Function) {
      super(o_spriteOrSpriteClass);
			init(f_onNextButtonPressed, f_onReadyButtonPressed);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // showNextButton
    //==================================================================
    public function showNextButton():void {
      showDisplayExclusive(mo_nextButton);
    }
    
    //==================================================================
    // showReadyButton
    //==================================================================
    public function showReadyButton():void {
      showDisplayExclusive(mo_readyButton);
    }
    
    //==================================================================
    // showWaitingToNextGameLabel
    //==================================================================
    public function showWaitingToNextGameLabel():void {
      showDisplayExclusive(mo_txfWaitingToNextGame);
    }
    
    //==================================================================
    // showWaitingToStartLabel
    //==================================================================
    public function showWaitingToStartLabel():void {
      showDisplayExclusive(mo_txfWaitingToStart);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mf_onNextButtonPressed = mf_onReadyButtonPressed = null;
      mo_nextButton = uninitButton(mo_nextButton);
      mo_readyButton = uninitButton(mo_readyButton);
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(f_onNextButtonPressed:Function, f_onReadyButtonPressed:Function):void {
      mo_txfWaitingToStart = getTextField("txf_waiting_to_start");
      mo_txfWaitingToNextGame = getTextField("txf_waiting_for_next_game");
      mo_nextButton = initButton2("next_button_display", onNextButtonPressed);
      mo_readyButton = initButton2("ready_button_display", onReadyButtonPressed);
      
      mf_onNextButtonPressed = f_onNextButtonPressed;
      mf_onReadyButtonPressed = f_onReadyButtonPressed;
    }
    
    //==================================================================
    // onNextButtonPressed
    //==================================================================
    private function onNextButtonPressed(o_button:Button, o_event:Event):void {
      mo_nextButton.enabled = false;
      if (mf_onNextButtonPressed is Function) {
        mf_onNextButtonPressed();
      }
    }
    
    //==================================================================
    // onReadyButtonPressed
    //==================================================================
    private function onReadyButtonPressed(o_button:Button, o_event:Event):void {
      mo_readyButton.enabled = false;
      if (mf_onReadyButtonPressed is Function) {
        mf_onReadyButtonPressed();
      }
    }
    
    //==================================================================
    // showDisplayExclusive
    //==================================================================
    private function showDisplayExclusive(o_displayToShow:Object):void {
      var ao_displays:Array = [
        mo_txfWaitingToNextGame,
        mo_txfWaitingToStart,
        mo_nextButton,
        mo_readyButton
      ];
      
      for each (var o_display:Object in ao_displays) {
        ShowDisplay(o_display, o_display == o_displayToShow);
      }
    }
  }
}