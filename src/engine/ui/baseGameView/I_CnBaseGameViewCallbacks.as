/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_CnBaseGameViewCallbacks

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          09-19-2018
  History:
    09-19-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.baseGameView {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  public interface I_CnBaseGameViewCallbacks extends I_BaseViewCallbacks {
    function gameViewOnBack(o_view:CnBaseGameView):void;
    function gameViewOnNext(o_view:CnBaseGameView):void;
    function gameViewOnThemeImagesComplete(o_view:CnBaseGameView):void;
    function gameViewOnWaitingViewNext(o_view:CnBaseGameView):void;
    function gameViewOnWaitingViewReady(o_view:CnBaseGameView):void;
  }
}