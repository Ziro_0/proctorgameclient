/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnResultsView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          10-24-2018
  ActionScript:  3.0
  Description:  
  History:
    10-24-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.baseGameView {

	import engine.common.baseView.BaseView;
	import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.core.I_Engine;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import flash.display.Sprite;
  import flash.text.TextField;
  import flib.utils.FDataBlock;
	
  internal class CnResultsView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: TextField
    private var mao_txfWinners:Array;
    
    //data: TextField
    private var mao_txfWinnerScores:Array;
    
    private var mo_engine:I_Engine;
    private var mo_resultsHeader:Sprite;
    private var mo_thanksHeader:Sprite;
    
    private var mo_txfNonWinnerScore:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnResultsView(o_spriteOrSpriteClass:Object, o_engine:I_Engine,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
			init(o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // showResultsHeader
    //==================================================================
    internal function showResultsHeader():void {
      ShowDisplay(mo_resultsHeader, true);
    }
    
    //==================================================================
    // showThanksHeader
    //==================================================================
    internal function showThanksHeader():void {
      ShowDisplay(mo_thanksHeader, true);
    }
    
    //==================================================================
    // winningPlayers (set)
    //==================================================================
    internal function set winningPlayers(ao_winnerPlayers:Array):void {
      var ao_data:Array = ao_winnerPlayers || [];
      
      if (ao_data.length > 1) {
        presentMultiplePlayers(ao_data);
      } else {
        presentOnePlayer(ao_data);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine):void {
      mo_engine = o_engine;
      initTextFields();
      mo_resultsHeader = getSprite("results_header_display");
      mo_thanksHeader = getSprite("results_thanks_header_display");
    }
    
    //==================================================================
    // initTextFields
    //==================================================================
    private function initTextFields():void {
      mao_txfWinners = [
        getTextField("txf_winner0"),
        getTextField("txf_winner1"),
        getTextField("txf_winner2")
      ];
      
      mao_txfWinnerScores = [
        getTextField("txf_score_winner0"),
        getTextField("txf_score_winner1"),
        getTextField("txf_score_winner2")
      ];
      
      mo_txfNonWinnerScore = getTextField("txf_non_winner_score");
    }
    
    //==================================================================
    // getPlayerName
    //==================================================================
    private function getPlayerName(s_playerId:String):String {
      const PLAYER_DEFAULT_NAME:String = "Player";
      var o_playerData:FDataBlock = mo_engine.getPlayerData(s_playerId);
      var o_userName:UserData = o_playerData ?
        o_playerData.getObject(PlayerStateKeys.USER_DATA) as UserData :
        null;
      return(o_userName ? o_userName.name : PLAYER_DEFAULT_NAME);
    }
    
    //==================================================================
    // hideNonWinnerTexts
    //==================================================================
    private function hideNonWinnerTexts():void {
      BaseView.ShowDisplay([
        mo_thanksHeader,
        mo_txfNonWinnerScore
      ], false);
    }
    
    //==================================================================
    // hideTexts
    //==================================================================
    private function hideTexts(ao_textfields:Array):void {
      BaseView.ShowDisplay(ao_textfields, false);
    }
    
    //==================================================================
    // hideWinnerTextArrays
    //==================================================================
    private function hideWinnerTextArrays():void {
      hideTexts(mao_txfWinners);
      hideTexts(mao_txfWinnerScores);
    }
    
    //==================================================================
    // presentMultiplePlayers
    //==================================================================
    private function presentMultiplePlayers(ao_playersData:Array):void {
      hideNonWinnerTexts();
      
      var o_block:FDataBlock = new FDataBlock();
      var u_index:uint = 0;
      var u_length:uint = Math.min(ao_playersData.length, mao_txfWinners.length);
      while (u_index < u_length) {
        o_block.data = ao_playersData[u_index];
        
        var s_playerId:String = o_block.getString("id");
        var i_points:int = o_block.getNumber("points");
        var s_playerName:String = getPlayerName(s_playerId);
        
        setText(mao_txfWinners[u_index], s_playerName);
        setText(mao_txfWinnerScores[u_index], "SCORE: " + i_points);
        
        u_index++;
      }
      
      while (u_index < mao_txfWinners.length) {
        setText(mao_txfWinners[u_index], "");
        setText(mao_txfWinnerScores[u_index], "");
        u_index++;
      }
    }
    
    //==================================================================
    // presentOnePlayer
    //==================================================================
    private function presentOnePlayer(ao_playersData:Array):void {
      hideWinnerTextArrays();
      
      var o_block:FDataBlock = new FDataBlock(ao_playersData[0]);
      var s_playerId:String = o_block.getString("id");
      var i_points:int = o_block.getNumber("points");
      
      if (i_points > 0) {
        setText(mo_txfNonWinnerScore, "SCORE: " + i_points);
      } else {
        hideTexts([ mo_txfNonWinnerScore ]);
      }
    }
    
    //==================================================================
    // setText
    //==================================================================
    private function setText(o_textfield:TextField, o_value:Object):void {
      if (o_textfield) {
        o_textfield.text = o_value != null ? o_value.toString() : "";
      }
    }
  }
}