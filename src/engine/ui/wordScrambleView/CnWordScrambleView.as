/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnWordScrambleView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-09-2018
  ActionScript:  3.0
  Description:  
  History:
    05-09-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.wordScrambleView {
  import engine.I_ClientEngine;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.data.CnDebugData;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.baseGameView.I_CnBaseGameViewCallbacks;
  import flash.display.DisplayObjectContainer;
  import flash.text.TextField;
  
  public class CnWordScrambleView extends CnBaseGameView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_answerInputLabel:InputLabel;
    private var mo_txfLabel:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnWordScrambleView(o_engine:I_ClientEngine,
    o_callbacks:I_CnBaseGameViewCallbacks = null) {
      var o_viewClass:Class = o_engine.app.getTemplateClass("WordScrambleDisplay_{d}");
      super(o_viewClass, o_engine, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // inputText (set)
    //==================================================================
    public function set inputText(s_text:String):void {
      mo_answerInputLabel.text = s_text;
    }
    
    //==================================================================
    // response (get)
    //==================================================================
    override public function get response():Object {
      return(mo_answerInputLabel.text);
    }
    
    //==================================================================
    // setEntry
    //==================================================================
    public function setEntry(u_index:uint, s_scramble:String):void {
      mo_txfLabel.text = u_index + "). " + s_scramble || "??";
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onInitContentDisplay
    //==================================================================
    override protected function onInitContentDisplay(
    o_contentContainer:DisplayObjectContainer):void {
      mo_txfLabel = getTextField("txf_label", "", o_contentContainer);
      mo_answerInputLabel = new InputLabel(o_contentContainer, null, "", "txf_answer");
      mo_answerInputLabel.initSoftKeyboardHandler(getEngine(), display);
      mo_answerInputLabel.debugDrawSoftKeyboardShapesEnabled = CnDebugData.SHOW_SOFT_KYBD_SHAPES;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}