/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnThemeImagesHelper

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          01-16-2019
  ActionScript:  3.0
  Description:  
  History:
    01-16-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.themeImagesHelper {
  import engine.I_ClientEngine;
  import engine.common.data.GameModels;
  import engine.common.data.ThemeComponentNames;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.ui.baseGameView.CnBaseGameView;
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flash.events.Event;
  import flib.utils.framesDelayer.FFramesDelayer;
  import flib.utils.framesDelayer.IFFramesDelayerCallbacks;

  public class CnThemeImagesHelper implements IFFramesDelayerCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FRAME_ID_BACK:uint = 1;
    private static const FRAME_ID_BANNER:uint = 2;
    private static const FRAME_ID_PANEL:uint = 3;
    private static const FRAME_ID_LOBBY_BABY_GAME_SELECTED:uint = 4;
    private static const FRAME_ID_LOBBY_BRIDAL_GAME_SELECTED:uint = 5;
    private static const FRAME_ID_RESULTS_BACK:uint = 6;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mau_frameIdOrder:Array;
    private var mo_engine:I_ClientEngine;
    private var mo_view:CnBaseGameView;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnThemeImagesHelper(o_view:CnBaseGameView, o_engine:I_ClientEngine) {
      mo_view = o_view;
      mo_engine = o_engine;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addThemeComponentsImages
    //==================================================================
    public function addThemeComponentsImages(s_gameModel:String):void {
      mau_frameIdOrder = [
        FRAME_ID_BACK,
        FRAME_ID_BANNER,
        FRAME_ID_PANEL,
        FRAME_ID_RESULTS_BACK
      ];
      
      if (s_gameModel == GameModels.BABY) {
        mau_frameIdOrder.push(FRAME_ID_LOBBY_BABY_GAME_SELECTED);
      } else if (s_gameModel == GameModels.BRIDAL) {
        mau_frameIdOrder.push(FRAME_ID_LOBBY_BRIDAL_GAME_SELECTED);
      }
      
      nextThemeComponentImage();
    }
    
    //==================================================================
    // frameDelayerOnComplete
    //==================================================================
    public function frameDelayerOnComplete(u_id:uint, o_data:Object):void {
      switch (u_id) {
        case FRAME_ID_BACK:
          addThemeComponentsImagesBack();
          nextThemeComponentImage();
          break;
          
        case FRAME_ID_BANNER:
          addThemeComponentsImagesBanner();
          nextThemeComponentImage();
          break;
          
        case FRAME_ID_PANEL:
          addThemeComponentsImagesPanel();
          nextThemeComponentImage();
          break;
          
        case FRAME_ID_LOBBY_BABY_GAME_SELECTED:
          addThemeComponentsImagesLobbyBabyGameSelected();
          nextThemeComponentImage();
          break;
          
        case FRAME_ID_LOBBY_BRIDAL_GAME_SELECTED:
          addThemeComponentsImagesLobbyBridalGameSelected();
          nextThemeComponentImage();
          break;
          
        case FRAME_ID_RESULTS_BACK:
          addThemeComponentsImagesResultBack();
          nextThemeComponentImage();
          break;
          
        default:
          mo_view.themeImagesHelperOnComplete();
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addThemeComponentsImage
    //==================================================================
    private function addThemeComponentsImage(s_themeComponentName:String,
    s_placeholderNodeName:String):void {
      var s_gameId:String = mo_engine.activeGameId;
      var o_image:DisplayObject = getThemeComponentsImage(s_gameId, s_themeComponentName);
      if (!o_image) {
        trace("CnThemeImagesHelper.addThemeComponentsImage. Warning: missing '" + s_themeComponentName +
          "' for game id: " + s_gameId);
      }
      
      var o_node:Sprite = initNode(s_placeholderNodeName);
      if (!o_node) {
        trace("CnThemeImagesHelper.addThemeComponentsImage. Warning: missing node '" +
          s_placeholderNodeName + "'.");
      }
      
      if (o_image && o_node) {
        scaleDisplayToFit(o_image);
        o_node.addChild(o_image);
      }
    }
    
    //==================================================================
    // addThemeComponentsImagesBack
    //==================================================================
    private function addThemeComponentsImagesBack():void {
      addThemeComponentsImage(ThemeComponentNames.BACK, "game_started_back_node");
    }
    
    //==================================================================
    // addThemeComponentsImagesBanner
    //==================================================================
    private function addThemeComponentsImagesBanner():void {
      addThemeComponentsImage(ThemeComponentNames.BANNER, "banner_node");
    }
    
    //==================================================================
    // addThemeComponentsImagesLobbyBabyGameSelected
    //==================================================================
    private function addThemeComponentsImagesLobbyBabyGameSelected():void {
      addThemeComponentsImage(ThemeComponentNames.LOBBY_BABY_GAME_SELECTED, "game_selected_back_node");
    }
    
    //==================================================================
    // addThemeComponentsImagesLobbyBridalGameSelected
    //==================================================================
    private function addThemeComponentsImagesLobbyBridalGameSelected():void {
      addThemeComponentsImage(ThemeComponentNames.LOBBY_BRIDAL_GAME_SELECTED, "game_selected_back_node");
    }
    
    //==================================================================
    // addThemeComponentsImagesPanel
    //==================================================================
    private function addThemeComponentsImagesPanel():void {
      addThemeComponentsImage(ThemeComponentNames.PANEL, "panel_node");
    }
    
    //==================================================================
    // addThemeComponentsImagesResultBack
    //==================================================================
    private function addThemeComponentsImagesResultBack():void {
      addThemeComponentsImage(ThemeComponentNames.RESULTS_BACK, "results_back_node");
    }
    
    //==================================================================
    // getThemeComponentsImage
    //==================================================================
    private function getThemeComponentsImage(s_gameId:String,
    s_themeComponentName:String):DisplayObject {
      var s_themeId:String = mo_engine.serverState.getString(ServerStateKeys.THEME_ID);
      var s_componentThemeId:String = getThemeIdFromComp(s_themeComponentName);
      return(mo_engine.themeComponentImages.getImage(s_themeId, s_componentThemeId, s_gameId,
        s_themeComponentName));
    }
    
    //==================================================================
    // getThemeIdFromComp
    //==================================================================
    private function getThemeIdFromComp(s_themeComponentName:String):String {
      var o_themeIdsByCompName:Object =
        mo_engine.serverState.getObject(ServerStateKeys.THEME_IDS_BY_COMP_NAME);
      return(o_themeIdsByCompName[s_themeComponentName]);
    }
    
    //==================================================================
    // initNode
    //==================================================================
    private function initNode(s_nodeName:String, o_container:DisplayObjectContainer = null):Sprite {
      var o_node:Sprite = mo_view.getSprite(s_nodeName, o_container);
      if (o_node) {
        //container nodes SHOULD only one child, which serves as a visual placeholder -
        // but remove all child displays, just in case
        o_node.removeChildren();
      }
      
      return(o_node);
    }
    
    //==================================================================
    // nextThemeComponentImage
    //==================================================================
    private function nextThemeComponentImage():void {
      if (mau_frameIdOrder.length == 0) {
        mo_view.themeImagesHelperOnComplete();
      } else {
        var u_frameId:uint = mau_frameIdOrder.shift();
        new FFramesDelayer(u_frameId, this);
      }
    }
    
    //==================================================================
    // scaleDisplayToFit
    //==================================================================
    private function scaleDisplayToFit(o_display:DisplayObject):void {
      o_display.scaleX = o_display.scaleY = 1 / mo_engine.app.deviceProfile.templateScale;
    }
  }
}