/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_CnToggleButtonCallbacks

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          01-15-2019
  History:
    01-15-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.toggleButtons {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  public interface I_CnToggleButtonCallbacks extends I_BaseViewCallbacks {
    function toggleButtonOnChange(o_toggleButton:CnToggleButton):void;
  }
}