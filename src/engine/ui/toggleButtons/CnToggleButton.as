/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnToggleButton

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          01-15-2019
  ActionScript:  3.0
  Description:  
  History:
    01-15-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.toggleButtons {

  import engine.I_ClientEngine;
  import engine.common.baseView.BaseView;
  import engine.common.data.ThemeComponentNames;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.utils.themeComponentImages.ThemeComponentImages;
  import flash.display.Bitmap;
  import flash.display.BitmapData;
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flash.events.Event;
	
  public class CnToggleButton extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_bmpOnImage:Bitmap;
    private var mo_bmpOffImage:Bitmap;
    private var mb_isSelected:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnToggleButton(o_node:Sprite, o_engine:I_ClientEngine,
    o_callbacks:I_CnToggleButtonCallbacks = null) {
      super(Sprite, o_callbacks);
			init(o_node, o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isSelected (get)
    //==================================================================
    public function get isSelected():Boolean {
      return(mb_isSelected);
    }
    
    //==================================================================
    // isSelected (set)
    //==================================================================
    public function set isSelected(b_value:Boolean):void {
      mb_isSelected = b_value;
      setImageVisible(mo_bmpOffImage, !mb_isSelected);
      setImageVisible(mo_bmpOnImage, mb_isSelected);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      uninitImages();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createButtonImage
    //==================================================================
    private function createButtonImage(o_engine:I_ClientEngine, s_themeComponentName:String):Bitmap {
      var o_bmpTarget:Bitmap = new Bitmap();
      
      var o_bmpSource:Bitmap = getSourceImage(o_engine, s_themeComponentName);
      if (o_bmpSource) {
        o_bmpTarget.bitmapData = o_bmpSource.bitmapData;
      }
      
      return(o_bmpTarget);
    }
    
    //==================================================================
    // getSourceImage
    //==================================================================
    private function getSourceImage(o_engine:engine.I_ClientEngine,
    s_themeComponentName:String):Bitmap {
      var o_themeComponentImages:ThemeComponentImages = o_engine.themeComponentImages;
      var o_themeIdsByCompName:Object =
        o_engine.serverState.getObject(ServerStateKeys.THEME_IDS_BY_COMP_NAME);
      var s_themeId:String = o_engine.serverState.getString(ServerStateKeys.THEME_ID);
      var s_componentThemeId:String = o_themeIdsByCompName[s_themeComponentName] as String;
      var s_gameId:String = o_engine.serverState.getString(ServerStateKeys.ACTIVE_GAME_ID);
      return(o_themeComponentImages.getImage(s_themeId, s_componentThemeId, s_gameId,
        s_themeComponentName));
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_node:Sprite, o_engine:I_ClientEngine):void {
      initDisplay(o_node);
      initOnButtonImage(o_node, o_engine);
      initOffButtonImage(o_node, o_engine);
      
      isSelected = false;
    }
    
    //==================================================================
    // initButtonImage
    //==================================================================
    private function initButtonImage(o_node:Sprite, o_engine:I_ClientEngine,
    s_themeComponentName:String):Bitmap {
      var o_bmpImage:Bitmap = createButtonImage(o_engine, s_themeComponentName);
      if (o_bmpImage) {
        initButtonSize(o_node, o_bmpImage);
        display.addChild(o_bmpImage);
      }
      return(o_bmpImage);
    }
    
    //==================================================================
    // initButtonSize
    //==================================================================
    private function initButtonSize(o_node:Sprite, o_bmpImage:Bitmap):void {
      if (o_bmpImage && o_node) {
        o_bmpImage.width = o_node.width;
        o_bmpImage.height = o_node.height;
      }
    }
    
    //==================================================================
    // initDisplay
    //==================================================================
    private function initDisplay(o_node:Sprite):void {
      var o_container:DisplayObjectContainer = o_node.parent;
      if (o_container) {
        var i_displayIndex:int = o_container.getChildIndex(o_node);
        o_container.removeChild(o_node);
        o_container.addChildAt(display, i_displayIndex);
      }
      
      display.x = o_node.x;
      display.y = o_node.y;
      
      initClickableObject(display, onClick);
    }
    
    //==================================================================
    // initOffButtonImage
    //==================================================================
    private function initOffButtonImage(o_node:Sprite, o_engine:I_ClientEngine):void {
      mo_bmpOffImage = initButtonImage(o_node, o_engine, ThemeComponentNames.RADIO_BUTTON_OFF);
    }
    
    //==================================================================
    // initOnButtonImage
    //==================================================================
    private function initOnButtonImage(o_node:Sprite, o_engine:I_ClientEngine):void {
      mo_bmpOnImage = initButtonImage(o_node, o_engine, ThemeComponentNames.RADIO_BUTTON_ON);
    }
    
    //==================================================================
    // notifyChange
    //==================================================================
    private function notifyChange():void {
      var o_callbacks:I_CnToggleButtonCallbacks = callbacks as I_CnToggleButtonCallbacks;
      if (o_callbacks) {
        o_callbacks.toggleButtonOnChange(this);
      }
    }
    
    //==================================================================
    // onClick
    //==================================================================
    private function onClick(o_event:Event):void {
      isSelected = !isSelected;
      notifyChange();
    }
    
    //==================================================================
    // setImageVisible
    //==================================================================
    private function setImageVisible(o_image:DisplayObject, b_isVisible:Boolean):void {
      if (o_image) {
        o_image.visible = b_isVisible;
      }
    }
    
    //==================================================================
    // uninitImages
    //==================================================================
    private function uninitImages():void {
      const DISPOSE_BITMAPDATA:Boolean = false;
      mo_bmpOffImage = uninitBitmap(mo_bmpOffImage, DISPOSE_BITMAPDATA);
      mo_bmpOnImage = uninitBitmap(mo_bmpOnImage, DISPOSE_BITMAPDATA);
    }
  }
}