/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnFillInBlankView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-27-2018
  ActionScript:  3.0
  Description:  
  History:
    07-27-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.fillInBlankView {
  import engine.I_ClientEngine;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.baseGameView.I_CnBaseGameViewCallbacks;
  import flash.display.DisplayObjectContainer;
  import flash.text.TextField;
	
  public class CnFillInBlankView extends CnBaseGameView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_txfLabel:TextField;
    private var mo_txfQuestion:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnFillInBlankView(o_engine:I_ClientEngine,
    o_callbacks:I_CnBaseGameViewCallbacks = null) {
      var o_viewClass:Class = o_engine.app.getTemplateClass("FillInBlankDisplay_{d}");
      super(o_viewClass, o_engine, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // questionText (set)
    //==================================================================
    public function set questionText(s_text:String):void {
      mo_txfQuestion.text = s_text || "";
    }
    
    //==================================================================
    // setEntry
    //==================================================================
    public function setEntry(u_index:uint, s_question:String):void {
      mo_txfLabel.text = "#" + u_index;
      mo_txfQuestion.text = s_question || "";
      primaryInputText = "";
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onInitContentDisplay
    //==================================================================
    override protected function onInitContentDisplay(
    o_contentContainer:DisplayObjectContainer):void {
      mo_txfLabel = getTextField("txf_label", null, o_contentContainer);
      mo_txfQuestion = getTextField("txf_question", null, o_contentContainer);
      initPrimaryInputLabel("txf_answer");
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}