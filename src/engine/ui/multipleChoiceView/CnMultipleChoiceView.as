/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnMultipleChoiceView

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          07-10-2018
  ActionScript:  3.0
  Description:  
  History:
    07-10-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.multipleChoiceView {

  import engine.I_ClientEngine;
  import engine.games.multipleChoice.CnMultipleChoiceGameChoice;
  import engine.games.multipleChoice.CnMultipleChoiceGameEntry;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.baseGameView.I_CnBaseGameViewCallbacks;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flash.text.TextField;
	
  public class CnMultipleChoiceView extends CnBaseGameView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: CnMultipleChoiceOption
    private var mao_choices:Array;
    private var mo_selectedOption:CnMultipleChoiceOption;
    
    private var mo_txfLabel:TextField;
    private var mo_txfQuestion:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnMultipleChoiceView(o_engine:I_ClientEngine,
    o_callbacks:I_CnBaseGameViewCallbacks = null) {
      var o_viewClass:Class = o_engine.app.getTemplateClass("MultipleChoiceDisplay_{d}");
      super(o_viewClass, o_engine, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enabled (set)
    //==================================================================
    override public function set enabled(b_value:Boolean):void {
      for each (var o_choiceOption:CnMultipleChoiceOption in mao_choices) {
        o_choiceOption.enabled = b_value;
      }
    }
    
    //==================================================================
    // response (get)
    //==================================================================
    override public function get response():Object {
      var i_selectedIndex:int = mao_choices.indexOf(mo_selectedOption);
      return(i_selectedIndex);
    }
    
    //==================================================================
    // selectCorrectOption
    //==================================================================
    public function selectCorrectOption(u_correctOptionIndex:uint):void {
      for (var u_index:uint = 0; u_index < mao_choices.length; u_index++) {
        var o_choiceOption:CnMultipleChoiceOption = mao_choices[u_index];
        if (u_index == u_correctOptionIndex) {
          o_choiceOption.isSelected = true;
          o_choiceOption.showDisabledFilter = false;
          mo_selectedOption = o_choiceOption;
        } else {
          o_choiceOption.isSelected = false;
          o_choiceOption.showDisabledFilter = true;
        }
        
        o_choiceOption.enabled = false;
      }
    }
    
    //==================================================================
    // setEntry
    //==================================================================
    public function setEntry(o_entry:CnMultipleChoiceGameEntry):void {
      if (o_entry) {
        fillContents(o_entry);
      } else {
        clearContents();
      }
      
      unselectAllChoiceOptions();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onOptionSelected
    //==================================================================
    internal function onOptionSelected(o_optionView:CnMultipleChoiceOption):void {
      if (mo_selectedOption == o_optionView) {
        return;
      }
      
      unselectCurrentlySelectedOption();
      mo_selectedOption = o_optionView;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    
    //==================================================================
    // onInitContentDisplay
    //==================================================================
    override protected function onInitContentDisplay(
    o_contentContainer:DisplayObjectContainer):void {
      mo_txfLabel = getTextField("txf_label", null, o_contentContainer);
      mo_txfQuestion = getTextField("txf_question", null, o_contentContainer);
      initChoices(o_contentContainer);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clearContents
    //==================================================================
    private function clearContents():void {
      for (var i_index:int = 0; i_index < mao_choices.length; i_index++) {
        var o_choiceOption:CnMultipleChoiceOption = mao_choices[i_index];
        o_choiceOption.visible = false;
      }
    }
    
    //==================================================================
    // fillChoices
    //==================================================================
    private function fillChoices(ao_choices:Array):void {
      var i_numVisibleOptions:int = Math.min(ao_choices.length, mao_choices.length);
      var i_index:int;
      var o_choiceOption:CnMultipleChoiceOption;
      
      for (i_index = 0; i_index < i_numVisibleOptions; i_index++) {
        o_choiceOption = mao_choices[i_index];
        o_choiceOption.visible = true;
        o_choiceOption.enabled = true;
        
        var o_entryChoice:CnMultipleChoiceGameChoice = ao_choices[i_index];
        o_choiceOption.label = o_entryChoice.text;
      }
      
      while (i_index < mao_choices.length) {
        o_choiceOption = mao_choices[i_index];
        o_choiceOption.visible = false;
        i_index++;
      }
    }
    
    //==================================================================
    // fillContents
    //==================================================================
    private function fillContents(o_entry:CnMultipleChoiceGameEntry):void {
      var i_questionNumber:int = o_entry.index + 1;
      mo_txfLabel.text = "Question " + i_questionNumber + ":";
      mo_txfQuestion.text = o_entry.question || "";
      fillChoices(o_entry.choices);
    }
    
    //==================================================================
    // getChoiceDisplay
    //==================================================================
    private function getChoiceDisplay(u_index:int,
    o_contentContainer:DisplayObjectContainer):Sprite {
      return(getSprite("choice" + u_index + "_display", o_contentContainer));
    }
    
    //==================================================================
    // initChoices
    //==================================================================
    private function initChoices(o_contentContainer:DisplayObjectContainer):void {
      mao_choices = [];
      
      var u_index:uint = 0;
      var o_display:Sprite = getChoiceDisplay(u_index, o_contentContainer);
      while (o_display) {
        mao_choices.push(new CnMultipleChoiceOption(o_display, this, getEngine()));
        u_index++;
        o_display = getChoiceDisplay(u_index, o_contentContainer);
      }
    }
    
    //==================================================================
    // unselectAllChoiceOptions
    //==================================================================
    private function unselectAllChoiceOptions():void {
      for each (var o_choiceOption:CnMultipleChoiceOption in mao_choices) {
        o_choiceOption.isSelected = false;
      }
      
      mo_selectedOption = null;
    }
    
    //==================================================================
    // unselectCurrentlySelectedOption
    //==================================================================
    private function unselectCurrentlySelectedOption():void {
      if (mo_selectedOption) {
        mo_selectedOption.isSelected = false;
      }
    }
  }
}