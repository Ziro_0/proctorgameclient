/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnMultipleChoiceOption

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          07-10-2018
  ActionScript:  3.0
  Description:  
  History:
    07-10-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.multipleChoiceView {

  import engine.I_ClientEngine;
  import engine.common.baseView.BaseView;
  import engine.ui.toggleButtons.CnToggleButton;
  import engine.ui.toggleButtons.I_CnToggleButtonCallbacks;
  import flash.events.Event;
  import flash.text.TextField;
	
  internal class CnMultipleChoiceOption extends BaseView implements I_CnToggleButtonCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_view:CnMultipleChoiceView;
    private var mo_toggleButton:CnToggleButton;
    private var mo_txfChoice:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnMultipleChoiceOption(o_spriteOrSpriteClass:Object,
    o_view:CnMultipleChoiceView, o_engine:I_ClientEngine) {
      super(o_spriteOrSpriteClass);
			init(o_view, o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isSelected (set)
    //==================================================================
    public function set isSelected(b_value:Boolean):void {
      mo_toggleButton.isSelected = b_value;
    }
    
    //==================================================================
    // label (set)
    //==================================================================
    public function set label(s_value:String):void {
      mo_txfChoice.text = s_value || "";
    }
    
    //==================================================================
    // toggleButtonOnChange
    //==================================================================
    public function toggleButtonOnChange(o_toggleButton:CnToggleButton):void {
      if (mo_view) {
        mo_view.onOptionSelected(this);
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_toggleButton.uninit();
      mo_view = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_view:CnMultipleChoiceView, o_engine:I_ClientEngine):void {
      mo_view = o_view;
      
      initToggleButton(o_engine);
      mo_txfChoice = getTextField("txf_choice");
      initHitNode();
    }
    
    //==================================================================
    // initHitNode
    //==================================================================
    private function initHitNode():void {
      initClickableObject("hit_node", onHitNodeClick);
    }
    
    //==================================================================
    // initToggleButton
    //==================================================================
    private function initToggleButton(o_engine:I_ClientEngine):void {
      mo_toggleButton = new CnToggleButton(getSprite("toggle_button_node"), o_engine, this);
    }
    
    //==================================================================
    // onHitNodeClick
    //==================================================================
    private function onHitNodeClick(o_event:Event):void {
      isSelected = true;
      if (mo_view) {
        mo_view.onOptionSelected(this);
      }
    }
  }
}