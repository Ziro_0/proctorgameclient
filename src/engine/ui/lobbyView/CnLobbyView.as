/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnLobbyView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-26-2018
  ActionScript:  3.0
  Description:  
  History:
    05-26-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.lobbyView {

  import engine.common.baseView.BaseView;
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.core.I_Engine;
  import engine.common.data.ThemeComponentNames;
  import engine.common.data.messageKeys.ServerStateKeys;
  import flash.display.DisplayObject;
  import flash.text.TextField;
	
  public class CnLobbyView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const NO_GAME_SELECTED_TEXT:String = "ADMIN HAS NOT YET SELECTED A GAME";
    private static const GAME_SELECTED_TEXT:String = "GAME SELECTED:\n";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    private var mo_txfSelectedGame:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnLobbyView(o_spriteOrSpriteClass:Object, o_engine:I_Engine,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
			init(o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // noSelectedGameText
    //==================================================================
    public function noSelectedGameText():void {
      selectedGameText = null;
    }
    
    //==================================================================
    // selectedGameText (set)
    //==================================================================
    public function set selectedGameText(s_value:String):void {
      if (s_value) {
        mo_txfSelectedGame.text = GAME_SELECTED_TEXT + s_value;
      } else {
        mo_txfSelectedGame.text = NO_GAME_SELECTED_TEXT;
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_engine = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createNoGameSelectedDisplay
    //==================================================================
    private function createNoGameSelectedDisplay():DisplayObject {
      var o_themeIdsByCompName:Object =
        mo_engine.serverState.getObject(ServerStateKeys.THEME_IDS_BY_COMP_NAME);
      var s_themeId:String = mo_engine.serverState.getString(ServerStateKeys.THEME_ID);
      const THEME_NAME:String = ThemeComponentNames.LOBBY_NO_GAME_SELECTED;
      var s_componentThemeId:String = o_themeIdsByCompName[THEME_NAME] as String;
      var s_gameId:String = mo_engine.serverState.getString(ServerStateKeys.ACTIVE_GAME_ID);
      return(mo_engine.themeComponentImages.getImage(s_themeId, s_componentThemeId, s_gameId, THEME_NAME));
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine):void {
      mo_engine = o_engine;
      initNoGameSelectedDisplay();
      initText();
    }
    
    //==================================================================
    // initNoGameSelectedDisplay
    //==================================================================
    private function initNoGameSelectedDisplay():void {
      var o_display:DisplayObject = createNoGameSelectedDisplay();
      if (o_display) {
        scaleDisplayToFit(o_display);
        display.addChildAt(o_display, 0);
      }
    }
    
    //==================================================================
    // initText
    //==================================================================
    private function initText():void {
      mo_txfSelectedGame = getTextField("txf_selected_game", NO_GAME_SELECTED_TEXT);
      mo_txfSelectedGame.mouseEnabled = false;
    }
    
    //==================================================================
    // scaleDisplayToFit
    //==================================================================
    private function scaleDisplayToFit(o_display:DisplayObject):void {
      o_display.scaleX = o_display.scaleY = 1 / mo_engine.app.deviceProfile.templateScale;
    }
  }
}