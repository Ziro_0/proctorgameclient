/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnLetterEntryView

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          07-16-2018
  ActionScript:  3.0
  Description:  
  History:
    07-16-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.lettersView {
  import engine.common.baseView.BaseView;
  import engine.common.core.I_Engine;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.data.CnDebugData;
  import flash.text.TextField;
	
  internal class CnLetterEntryView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_parentView:BaseView;
    private var mo_answerInputLabel:InputLabel;
    private var mo_txfLetter:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnLetterEntryView(o_spriteOrSpriteClass:Object, o_engine:I_Engine, o_parentView:BaseView) {
      super(o_spriteOrSpriteClass);
			init(o_engine, o_parentView);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // letter (get)
    //==================================================================
    internal function get letter():String {
      return(mo_txfLetter.text);
    }
    
    //==================================================================
    // letter (set)
    //==================================================================
    internal function set letter(s_value:String):void {
      var s_char:String = s_value ? s_value.charAt(0).toUpperCase() : "";
      mo_txfLetter.text = s_char;
      mo_answerInputLabel.text = "";
    }
    
    //==================================================================
    // respose (get)
    //==================================================================
    internal function get respose():String {
      return(mo_answerInputLabel.text);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine, o_parentView:BaseView):void {
      mo_parentView = o_parentView;
      
      mo_txfLetter = getTextField("txf_letter");
      
      mo_answerInputLabel = new InputLabel(this, null, "", "txf_input");
      mo_answerInputLabel.initSoftKeyboardHandler(o_engine, mo_parentView.display);
      mo_answerInputLabel.debugDrawSoftKeyboardShapesEnabled = CnDebugData.SHOW_SOFT_KYBD_SHAPES;
    }
  }
}