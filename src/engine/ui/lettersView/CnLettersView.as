/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnLettersView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-16-2018
  ActionScript:  3.0
  Description:  
  History:
    07-16-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.lettersView {
  import engine.I_ClientEngine;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.baseGameView.I_CnBaseGameViewCallbacks;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
	
  public class CnLettersView extends CnBaseGameView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: CnLetterEntryView
    private var mao_letterEntryViews:Array;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnLettersView(o_engine:I_ClientEngine,
    o_callbacks:I_CnBaseGameViewCallbacks = null) {
      var o_viewClass:Class = o_engine.app.getTemplateClass("LettersDisplay_{d}");
      super(o_viewClass, o_engine, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enabled (set)
    //==================================================================
    override public function set enabled(b_value:Boolean):void {
      if (!b_value) {
        display.stage.focus = null;
      }
      
      enableEntries(b_value);
    }
    
    //==================================================================
    // response (get)
    //==================================================================
    override public function get response():Object {
      var as_words:Array = [];
      as_words.length = mao_letterEntryViews.length;
      
      for (var u_index:int = 0; u_index < mao_letterEntryViews.length; u_index++) {
        var o_entryView:CnLetterEntryView = mao_letterEntryViews[u_index];
        var s_response:String = o_entryView.respose as String;
        if (s_response) {
          as_words[u_index] = o_entryView.letter + s_response;
        }
      }
      
      return(as_words);
    }
    
    //==================================================================
    // setEntry
    //==================================================================
    public function setEntry(u_index:uint, s_word:String):void {
      setLetterViewEntries(s_word);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onInitContentDisplay
    //==================================================================
    override protected function onInitContentDisplay(
    o_contentContainer:DisplayObjectContainer):void {
      showDisabledFilter = false;
      initLetterEntries(o_contentContainer);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableEntries
    //==================================================================
    private function enableEntries(b_enabled:Boolean):void {
      for each (var o_entryView:CnLetterEntryView in mao_letterEntryViews) {
        o_entryView.enabled = b_enabled;
      }
    }
    
    //==================================================================
    // getLetterEntryViewDisplay
    //==================================================================
    private function getLetterEntryViewDisplay(u_index:uint,
    o_contentContainer:DisplayObjectContainer):Sprite {
      return(getSprite("letter_entry" + u_index + "_display", o_contentContainer));
    }
    
    //==================================================================
    // initLetterEntries
    //==================================================================
    private function initLetterEntries(o_contentContainer:DisplayObjectContainer):void {
      mao_letterEntryViews = [];
      
      var u_index:uint;
      var o_display:Sprite = getLetterEntryViewDisplay(u_index, o_contentContainer);
      while (o_display) {
        var o_entryView:CnLetterEntryView = new CnLetterEntryView(o_display, getEngine(), this);
        o_entryView.visible = false;
        mao_letterEntryViews.push(o_entryView);
        o_display = getLetterEntryViewDisplay(++u_index, o_contentContainer);
      }
    }
    
    //==================================================================
    // setLetterViewEntries
    //==================================================================
    private function setLetterViewEntries(s_word:String):void {
      var u_numLetters:uint = s_word ? s_word.length : 0;
      var u_maxIndex:uint = Math.min(u_numLetters, mao_letterEntryViews.length);
      var o_entryView:CnLetterEntryView;
      var u_index:uint;
      
      while (u_index < u_maxIndex) {
        o_entryView = mao_letterEntryViews[u_index] as CnLetterEntryView;
        o_entryView.visible = true;
        o_entryView.letter = s_word.charAt(u_index);
        u_index++;
      }
      
      while (u_index < mao_letterEntryViews.length) {
        o_entryView = mao_letterEntryViews[u_index] as CnLetterEntryView;
        o_entryView.visible = false;
        u_index++;
      }
    }
  }
}