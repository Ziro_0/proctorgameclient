/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnGameFactory

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-29-2018
  ActionScript:  3.0
  Description:  
  History:
    05-29-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.gameManager {
  import engine.I_ClientEngine;
  import engine.common.data.GameIdToTypesMap;
  import engine.common.data.GameTypes;
  import engine.games.CnGame;
  import engine.games.fillInBlank.CnFillInBlankGame;
  import engine.games.letters.CnLettersGame;
  import engine.games.multipleChoice.CnMultipleChoiceGame;
  import engine.games.unscramble.CnUnscrambleGame;

  public class CnGameFactory {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // Create
    //==================================================================
    public static function Create(o_engine:I_ClientEngine, s_gameId:String):CnGame {
      var s_gameType:String = GameIdToTypesMap.GetGameTypeFromId(s_gameId);
      var o_game:CnGame;
      
      switch (s_gameType) {
        case GameTypes.FILL_IN_BLANK:
          o_game = new CnFillInBlankGame(o_engine, s_gameId);
          break;
        
        case GameTypes.LETTERS:
          o_game = new CnLettersGame(o_engine, s_gameId);
          break;
          
        case GameTypes.MULTIPLE_CHOICE:
          o_game = new CnMultipleChoiceGame(o_engine, s_gameId);
          break;
          
        case GameTypes.UNSCRAMBLE:
          o_game = new CnUnscrambleGame(o_engine, s_gameId);
          break;
          
        default:
          o_game = null;
      }
      
      return(o_game);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}