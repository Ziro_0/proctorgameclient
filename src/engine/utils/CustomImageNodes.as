/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CustomImageNodes

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          08-21-2018
  ActionScript:  3.0
  Description:  
  History:
    08-21-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;

  public final class CustomImageNodes {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CustomImageNodes() {
      super();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // processNodes
    //==================================================================
    public function processNodes(o_displayContainer:DisplayObjectContainer, o_imagesByKey:Object,
    s_deviceProfileId:String):void {
      if (!o_displayContainer || o_imagesByKey == null || !s_deviceProfileId) {
        return;
      }
      
      const NODE_PREFIX:String = "node_";
      const IMAGE_KEY_OFFSET:int = NODE_PREFIX.length;
      const ASPECT_RATIO_CODE:String = "$ar";
      
      for (var i_index:int = 0; i_index < o_displayContainer.numChildren; i_index++) {
        var o_node:Sprite = o_displayContainer.getChildAt(i_index) as Sprite;
        if (!o_node) {
          continue;
        }
        
        var s_name:String = o_node.name;
        if (s_name.indexOf(NODE_PREFIX) != 0) {
          continue;
        }
        
        if (o_imagesByKey == null) {
          o_node.visible = false;
          continue;
        }
        
        var s_imageKeyTemplate:String = s_name.substr(IMAGE_KEY_OFFSET);
        var s_imageKey:String = s_imageKeyTemplate.replace(ASPECT_RATIO_CODE, s_deviceProfileId);
        var o_imageDisplay:DisplayObject = o_imagesByKey[s_imageKey] as DisplayObject;
        if (!o_imageDisplay) {
          o_node.visible = false;
          continue;
        }
        
        o_node.removeChildren();
        o_node.addChild(o_imageDisplay);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}