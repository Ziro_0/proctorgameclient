/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ByteArrayImagesLoader

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          08-13-2018
  ActionScript:  3.0
  Description:  
  History:
    08-13-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  import flash.display.Loader;
  import flash.display.LoaderInfo;
  import flash.events.ErrorEvent;
  import flash.events.Event;
  import flash.system.ApplicationDomain;
  import flash.system.LoaderContext;
  import flash.utils.ByteArray;

  public class ByteArrayImagesLoader {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: Object {
    //  key: String
    //  data: ByteArray
    //}
    private var mao_queuedByteArrays:Array;
    
    //key: String
    //data: Loader
    private var mo_loadersByKey:Object;
    
    private var mo_callbacks:I_ByteArrayImagesLoaderCallbacks;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ByteArrayImagesLoader(o_callbacks:I_ByteArrayImagesLoaderCallbacks) {
      super();
      
      mo_callbacks = o_callbacks;
      mao_queuedByteArrays = [];
      mo_loadersByKey = { };
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // add
    //==================================================================
    public function add(s_key:String, o_byteArray:ByteArray):Boolean {
      if (!s_key || !o_byteArray) {
        return(false);
      }
      
      mao_queuedByteArrays.push({
        key: s_key,
        data: o_byteArray
      });
      
      return(true);
    }
    
    //==================================================================
    // getLoader
    //==================================================================
    public function getLoader(s_key:String):Loader {
      if (s_key && s_key in mo_loadersByKey) {
        return(mo_loadersByKey[s_key] as Loader);
      }
      
      return(null);
    }
    
    //==================================================================
    // loadersByKey (get)
    //==================================================================
    /**
     * key: String
     * data: Loader
    */
    public function get loadersByKey():Object {
      return(mo_loadersByKey);
    }
    
    //==================================================================
    // load
    //==================================================================
    public function load():Boolean {
      return(loadNext());
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      unload();
      mo_callbacks = null;
    }
    
    //==================================================================
    // unload
    //==================================================================
    public function unload():void {
      mao_queuedByteArrays.length = 0;
      uninitLoaders();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // loadNext
    //==================================================================
    private function loadNext():Boolean {
      if (mao_queuedByteArrays.length == 0) {
        notifyComplete();
        return(false);
      }
      
      var o_loader:Loader = new Loader();
      o_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderInfoComplete, false, 0, true);
      o_loader.contentLoaderInfo.addEventListener(ErrorEvent.ERROR, onLoaderInfoError, false, 0, true);
      
      var o_loadItem:Object = mao_queuedByteArrays.shift();
      var o_byteArray:ByteArray = o_loadItem.data as ByteArray;
      
      try {
        o_loader.loadBytes(o_byteArray, new LoaderContext(false, ApplicationDomain.currentDomain));
        
        var s_key:String = o_loadItem.key as String;
        mo_loadersByKey[s_key] = o_loader;
      } catch (o_error:Error) {
        trace("ByteArrayImagesLoader.loadNext. Error: " + o_error);
        //try to load the next item, if there is one
        return(loadNext());
      }
      
      return(true);
    }
    
    //==================================================================
    // notifyComplete
    //==================================================================
    private function notifyComplete():void {
      if (mo_callbacks) {
        mo_callbacks.byteArrayImagesLoaderComplete(this);
      }
    }
    
    //==================================================================
    // onLoaderInfoComplete
    //==================================================================
    private function onLoaderInfoComplete(o_event:Event):void {
      uninitLoaderInfo(o_event.currentTarget as LoaderInfo);
      loadNext();
    }
    
    //==================================================================
    // onLoaderInfoError
    //==================================================================
    private function onLoaderInfoError(o_event:ErrorEvent):void {
      trace("ByteArrayImagesLoader.onLoaderInfoError. " + o_event.text);
      loadNext();
    }
    
    //==================================================================
    // uninitLoaderInfo
    //==================================================================
    private function uninitLoaderInfo(o_loaderInfo:LoaderInfo):void {
      if (o_loaderInfo) {
        o_loaderInfo.removeEventListener(Event.COMPLETE, onLoaderInfoComplete);
        o_loaderInfo.removeEventListener(ErrorEvent.ERROR, onLoaderInfoError);
      }
    }
    
    //==================================================================
    // uninitLoaders
    //==================================================================
    private function uninitLoaders():void {
      for (var s_key:String in mo_loadersByKey) {
        var o_loader:Loader = mo_loadersByKey[s_key] as Loader;
        if (o_loader) {
          uninitLoaderInfo(o_loader.loaderInfo);
          
          try {
            o_loader.close();
          } catch (o_error:Error) {
            //just to catch an already closed loader
          }
          
          o_loader.unload();
        }
        
        delete mo_loadersByKey[s_key];
      }
    }
  }
}