/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnComponentThemeImageFileWriter

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-27-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.themeImageFileWriter {
  import engine.common.data.CustomThemePath;
  import engine.common.utils.ThemeImagePath;
  import engine.common.utils.themeImagesDataLoader.ThemeImagesData;
  import engine.data.CnDesignData;
  import flash.events.Event;
  import flash.events.IOErrorEvent;
  import flash.events.OutputProgressEvent;
  import flash.events.ProgressEvent;
  import flash.filesystem.File;
  import flash.filesystem.FileMode;
  import flash.filesystem.FileStream;
  import flash.utils.ByteArray;
  import flib.utils.FArrayUtils;
  import flib.utils.FDataBlock;

  public final class CnComponentThemeImageFileWriter {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_CnComponentThemeImageFileWriterCallbacks;
    
    private var mo_readOnlyThemeImagePath:ThemeImagePath;
    private var mo_customThemeImagePath:ThemeImagePath;
    
    //data: FileStream
    private var mao_openStreams:Array;
    
    //data: {
    //  url: String
    //  data: ByteArray
    //}
    private var mao_queue:Array;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnComponentThemeImageFileWriter(s_deviceProfileId:String,
    o_callbacks:I_CnComponentThemeImageFileWriterCallbacks = null, s_fileExt:String = null) {
      super();
      
      mao_queue = [ ];
      mao_openStreams = [ ];
      
      mo_readOnlyThemeImagePath = createThemeImagePath(CnDesignData.THEMES_BASE_URL,
        s_deviceProfileId, s_fileExt);
      mo_customThemeImagePath  = createThemeImagePath(CustomThemePath.CUSTOM_BASE_PATH,
        s_deviceProfileId, s_fileExt);
      
      callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_CnComponentThemeImageFileWriterCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_CnComponentThemeImageFileWriterCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // dispose
    //==================================================================
    public function dispose():void {
      callbacks = null;
      closeAllStreams();
      mao_queue.length = 0;
    }
    
    //==================================================================
    // run
    //==================================================================
    public function run(o_imageDataByKey:Object):void {
      if (!o_imageDataByKey) {
        return;
      }
      
      for (var s_imageKey:String in o_imageDataByKey) {
        var o_data:ThemeImagesData = o_imageDataByKey[s_imageKey] as ThemeImagesData;
        if (!o_data || !o_data.themeComponentName || !o_data.themeId) {
          trace("CnComponentThemeImageFileWriter.run. Warning: Invalid data for key: " + s_imageKey);
          continue;
        }
        
        var o_path:ThemeImagePath = getThemeImagePath(o_data.themeComponentId);
        var s_fileUrl:String = o_path.createGameOrNonGameFileUrl(o_data.themeId, o_data.gameId,
          o_data.themeComponentName);
        
        if (!s_fileUrl) {
          trace("CnComponentThemeImageFileWriter.run. Warning: Invalid file URL for: " +
            "theme id: "  + o_data.themeId
            + "; game id: " +  o_data.gameId
            + "; theme component name: " + o_data.themeComponentName
            + "; theme component id: " +  o_data.themeComponentId);
          continue;
        }
        
        mao_queue.push({
          url: s_fileUrl,
          data: o_data.imageData
        });
      }
      
      writeNextFile();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // closeAllStreams
    //==================================================================
    private function closeAllStreams():void {
      while (mao_openStreams.length) {
        closeStream(mao_openStreams[0] as FileStream);
      }
    }
    
    //==================================================================
    // closeStream
    //==================================================================
    private function closeStream(o_stream:FileStream):void {
      if (!o_stream) {
        return;
      }
      
      o_stream.removeEventListener(Event.COMPLETE, onStreamComplete);
      o_stream.removeEventListener(ProgressEvent.PROGRESS, onStreamOutputProgress);
      o_stream.removeEventListener(IOErrorEvent.IO_ERROR, onStreamError);
      o_stream.close();
      
      FArrayUtils.Remove(mao_openStreams, o_stream);
    }
    
    //==================================================================
    // complete
    //==================================================================
    private function complete():void {
      if (mo_callbacks) {
        mo_callbacks.themeImageFileWriteOnComplete(this);
      }
    }
    
    //==================================================================
    // createThemeImagePath
    //==================================================================
    private function createThemeImagePath(s_basePath:String, s_deviceProfileId:String,
    s_fileExt:String):ThemeImagePath {
      var o_path:ThemeImagePath = new ThemeImagePath(s_basePath, s_deviceProfileId, s_fileExt);
      o_path.isApplicationStorageMode = true;
      return(o_path);
    }
    
    //==================================================================
    // getThemeImagePath
    //==================================================================
    private function getThemeImagePath(s_themeComponentId:String):ThemeImagePath {
      return(CustomThemePath.IsCustom(s_themeComponentId) ?
        mo_customThemeImagePath :
        mo_readOnlyThemeImagePath);
    }
    
    //==================================================================
    // openFile
    //==================================================================
    private function openFile(s_fileUrl:String):File {
      var o_file:File;
      
      try {
        o_file = new File(s_fileUrl);
      } catch (o_error:Error) {
        trace("CnComponentThemeImageFileWriter.openFile. Error: Invalid url: " + s_fileUrl +
          ", error: " + o_error);
      }
      
      return(o_file);
    }
    
    //==================================================================
    // openStream
    //==================================================================
    private function openStream(o_file:File):FileStream {
      var o_stream:FileStream = new FileStream();
      
      try {
        o_stream.openAsync(o_file, FileMode.UPDATE);
      } catch (o_error:Error) {
        trace("CnComponentThemeImageFileWriter.openStream. Error: Couldn't open stream for file with " +
          "url: " + o_file.url + ", error: " + o_error);
        return(null);
      }
      
      o_stream.addEventListener(Event.COMPLETE, onStreamComplete, false, 0, true);
      o_stream.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, onStreamOutputProgress,
        false, 0, true);
      o_stream.addEventListener(IOErrorEvent.IO_ERROR, onStreamError, false, 0, true);
      
      mao_openStreams.push(o_stream);
      
      return(o_stream);
    }
    
    //==================================================================
    // onStreamComplete
    //==================================================================
    private function onStreamComplete(o_event:Event):void {
      trace("CnComponentThemeImageFileWriter.onStreamComplete");
      var o_stream:FileStream = o_event.currentTarget as FileStream;
      closeStream(o_stream);
      writeNextFile();
    }
    
    //==================================================================
    // onStreamError
    //==================================================================
    private function onStreamError(o_event:IOErrorEvent):void {
      trace("CnComponentThemeImageFileWriter.onStreamError. Error: " + o_event.text);
      writeNextFile();
    }
    
    //==================================================================
    // onStreamOutputProgress
    //==================================================================
    private function onStreamOutputProgress(o_event:OutputProgressEvent):void {
      trace("CnComponentThemeImageFileWriter.onStreamOutputProgress. " + o_event.bytesPending + " / " +
        o_event.bytesTotal);
    }
    
    //==================================================================
    // shouldFileBeUpdated
    //==================================================================
    private function shouldFileBeUpdated(o_file:File, o_data:ByteArray):Boolean {
      return(
        !o_file.exists ||
        o_file.size != o_data.length);
    }
    
    //==================================================================
    // writeFile
    //==================================================================
    private function writeFile(s_fileUrl:String, o_data:ByteArray):void {
      var o_file:File = openFile(s_fileUrl);
      if (!o_file) {
        writeNextFile();
        return;
      }
      
      if (!shouldFileBeUpdated(o_file, o_data)) {
        writeNextFile();
        return;
      }
      
      trace("CnComponentThemeImageFileWriter.writeFile. Writing native file: " + o_file.nativePath);
      
      var o_stream:FileStream = openStream(o_file);
      if (!o_stream) {
        writeNextFile();
        return;
      }
      
      if (!writeStreamBytes(o_stream, o_file, o_data)) {
        closeStream(o_stream);
        writeNextFile();
      }
    }
    
    //==================================================================
    // writeStreamBytes
    //==================================================================
    private function writeStreamBytes(o_stream:FileStream, o_file:File, o_data:ByteArray):Boolean {
      try {
        o_stream.writeBytes(o_data, 0, o_data.length);
      } catch (o_error:Error) {
        trace("CnComponentThemeImageFileWriter.writeStreamBytes. Error: " +
          "Couldn't write stream for file with url: " + o_file.url + ", error: " + o_error);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // writeNextFile
    //==================================================================
    private function writeNextFile():void {
      if (!mao_queue.length) {
        complete();
        return;
      }
      
      var o_block:FDataBlock = new FDataBlock(mao_queue.shift());
      writeFile(o_block.getString("url"), o_block.getObject("data") as ByteArray);
    }
  }
}