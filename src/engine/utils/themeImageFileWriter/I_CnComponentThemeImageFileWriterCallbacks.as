/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_CnComponentThemeImageFileWriterCallbacks

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:          05-29-2019
  ActionScript:  3.0
  Description:  
  History:
    05-29-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.themeImageFileWriter {
  
  public interface I_CnComponentThemeImageFileWriterCallbacks {
    function themeImageFileWriteOnComplete(o_writer:CnComponentThemeImageFileWriter):void;
  }
}