/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_ByteArrayImagesLoaderCallbacks

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          08-14-2018
  Description:  
  History:
    08-14-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  
  public interface I_ByteArrayImagesLoaderCallbacks {
    function byteArrayImagesLoaderComplete(o_loader:ByteArrayImagesLoader):void;
  }
}