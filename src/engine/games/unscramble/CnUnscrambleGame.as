/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnUnscrambleGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-29-2018
  ActionScript:  3.0
  Description:  
  History:
    05-29-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.games.unscramble {

  import engine.I_ClientEngine;
  import engine.games.CnGame;
  import engine.ui.wordScrambleView.CnWordScrambleView;
  import flash.display.Sprite;
	
  public class CnUnscrambleGame extends CnGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_wordScrambleView:CnWordScrambleView;
    private var mo_currentEntry:CnUnscrambleGameEntry;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnUnscrambleGame(o_engine:I_ClientEngine, s_gameId:String) {
      super(o_engine, s_gameId);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // setEntry
    //==================================================================
    override public function setEntry(u_index:uint):void {
      var o_engine:I_ClientEngine = getEngine();
      if (!o_engine) {
        trace("CnUnscrambleGame.setEntry. Warning: Client engine not set.");
        return;
      }
      
      var ao_entries:Array = getEntries();
      var o_entry:Object = ao_entries[u_index];
      if (!o_entry) {
        mo_currentEntry = null;
        mo_wordScrambleView.visible = false;
        return;
      }
      
      mo_currentEntry = new CnUnscrambleGameEntry(o_entry, u_index);
      mo_wordScrambleView.setEntry(u_index + 1, mo_currentEntry.scramble);
      mo_wordScrambleView.inputText = "";
      mo_wordScrambleView.visible = true;
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_wordScrambleView = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initWordScrambleDisplay();
    }
    
    //==================================================================
    // initWordScrambleDisplay
    //==================================================================
    private function initWordScrambleDisplay():void {
      mo_wordScrambleView = new CnWordScrambleView(getEngine(), this);
      addGameView(mo_wordScrambleView, false);
    }
  }
}