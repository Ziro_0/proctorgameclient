/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnFillInBlankGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-27-2018
  ActionScript:  3.0
  Description:  
  History:
    07-27-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.games.fillInBlank {

  import engine.I_ClientEngine;
  import engine.games.CnGame;
  import engine.ui.fillInBlankView.CnFillInBlankView;
  import flash.display.Sprite;
	
  public class CnFillInBlankGame extends CnGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_fillInBlankView:CnFillInBlankView;
    private var mo_currentEntry:CnFillInBlankGameEntry;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnFillInBlankGame(o_engine:I_ClientEngine, s_gameId:String) {
      super(o_engine, s_gameId);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // setEntry
    //==================================================================
    override public function setEntry(u_index:uint):void {
      var o_engine:I_ClientEngine = getEngine();
      if (!o_engine) {
        trace("CnFillInBlankGame.setEntry. Warning: Client engine not set.");
        return;
      }
      
      var ao_entries:Array = getEntries();
      var o_entry:Object = ao_entries[u_index];
      if (!o_entry) {
        mo_currentEntry = null;
        mo_fillInBlankView.visible = false;
        return;
      }
      
      mo_currentEntry = new CnFillInBlankGameEntry(o_entry, u_index);
      mo_fillInBlankView.setEntry(u_index + 1, mo_currentEntry.question);
      mo_fillInBlankView.primaryInputText = "";
      mo_fillInBlankView.visible = true;
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_fillInBlankView = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initDisplay();
    }
    
    //==================================================================
    // initDisplay
    //==================================================================
    private function initDisplay():void {
      mo_fillInBlankView = new CnFillInBlankView(getEngine(), this);
      addGameView(mo_fillInBlankView, false);
    }
  }
}