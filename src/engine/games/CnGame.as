/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-29-2018
  ActionScript:  3.0
  Description:  
  History:
    05-29-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.games {
  import engine.I_ClientEngine;
  import engine.common.baseView.BaseView;
  import engine.common.data.ClientMessageTypes;
  import engine.common.data.MessageProperties;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.messageKeys.ClientNextKeys;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.utils.messages.Message;
  import engine.ui.baseGameView.CnBaseGameView;
  import engine.ui.baseGameView.I_CnBaseGameViewCallbacks;
  import engine.ui.baseGameView.ResultsHeaders;
  import flash.display.Sprite;

  public class CnGame implements I_CnBaseGameViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_gameView:CnBaseGameView;
    
    private var mo_engine:I_ClientEngine;
    private var ms_gameId:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnGame(o_engine:I_ClientEngine, s_gameId:String) {
      super();
      mo_engine = o_engine;
      ms_gameId = ms_gameId;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // gameBgElementsVisible (set)
    //==================================================================
    public function set gameBgElementsVisible(b_value:Boolean):void {
      var ao_displays:Array = [
        mo_gameView.getSprite(CnBaseGameView.GAME_STARTED_BACK_NODE_NAME),
        mo_gameView.getSprite(CnBaseGameView.PANEL_NODE_NAME),
        mo_gameView.getSprite(CnBaseGameView.BANNER_NODE_NAME)
      ];
      
      BaseView.ShowDisplay(ao_displays, b_value);
    }
    
    //==================================================================
    // gameId (get)
    //==================================================================
    public function get gameId():String {
      return(ms_gameId);
    }
    
    //==================================================================
    // gameView (get)
    //==================================================================
    public function get gameView():CnBaseGameView {
      return(mo_gameView);
    }
    
    //==================================================================
    // gameViewOnBack
    //==================================================================
    public function gameViewOnBack(o_view:CnBaseGameView):void {
    }
    
    //==================================================================
    // gameViewOnNext
    //==================================================================
    public function gameViewOnNext(o_view:CnBaseGameView):void {
      var o_engine:I_ClientEngine = getEngine();
      var o_message:Object = Message.Create(ClientMessageTypes.NEXT);
      o_message[ClientNextKeys.CLIENT_ID] = o_engine.playerId;
      o_message[ClientNextKeys.RESPONSE] = onGetRoundResponse();
      o_engine.sendMessageToServer(o_message);
      
      setNextEntry();
    }
    
    //==================================================================
    // gameViewOnThemeImagesComplete
    //==================================================================
    public function gameViewOnThemeImagesComplete(o_view:CnBaseGameView):void {
      notifyThemeImagesComplete();
      mo_gameView.visible = true;
      
      mo_gameView.hideAllDisplayModes();
      
      if (mo_engine.isWaitingForNextGame) {
        var ao_winningPlayers:Array = mo_engine.winningPlayers;
        if (ao_winningPlayers) {
          showResultsDisplay(ao_winningPlayers);
        }
      }
    }
    
    //==================================================================
    // gameViewOnWaitingViewNext
    //==================================================================
    public function gameViewOnWaitingViewNext(o_view:CnBaseGameView):void {
      if (!mo_gameView) {
        return;
      }
      
      mo_gameView.showWaitingDisplay(CnBaseGameView.WAITING_MODE_READY_BUTTON);
      gameBgElementsVisible = true;
      BaseView.ShowDisplay(mo_gameView.getSprite(CnBaseGameView.GAME_SELECTED_BACK_NODE_NAME), false);
      BaseView.ShowDisplay(mo_gameView.getSprite(CnBaseGameView.GAME_STARTED_BACK_NODE_NAME), true);
      BaseView.ShowDisplay(mo_gameView.getSprite(CnBaseGameView.RESULTS_BACK_NODE_NAME), false);
    }
    
    //==================================================================
    // gameViewOnWaitingViewReady
    //==================================================================
    public function gameViewOnWaitingViewReady(o_view:CnBaseGameView):void {
      if (mo_gameView) {
        mo_gameView.showWaitingDisplay(CnBaseGameView.WAITING_MODE_START_LABEL);
        notifyServerThatClientIsReadyToStartGame();
      }
    }
    
    //==================================================================
    // getEngine
    //==================================================================
    public function getEngine():I_ClientEngine {
      return(mo_engine);
    }
    
    //==================================================================
    // hideWaitingDisplay
    //==================================================================
    public function hideWaitingDisplay():void {
      if (mo_gameView) {
        mo_gameView.hideWaitingDisplay();
      }
    }
    
    //==================================================================
    // onRoundTimerUpdate
    //==================================================================
    public function onRoundTimerUpdate(i_elapsedSeconds:int, i_maxSeconds:int):void {
      if (!mo_gameView) {
        return;
      }
      
      var i_secondsRemaining:int = i_maxSeconds - i_elapsedSeconds;
      if (i_secondsRemaining > 0) {
        //time remaining - gameplay still active
        mo_gameView.timerView.visible = true;
        mo_gameView.timerValue = i_secondsRemaining;
      } else {
        //time expired - gameplay stops
        mo_gameView.enabled = false;
        mo_gameView.timerView.visible = false;
      }
    }
    
    //==================================================================
    // setEntry
    //==================================================================
    /**
     * This method is meant to be overridder.
     * @param u_index - The index of the entry being set for the round.
     */
    public function setEntry(u_index:uint):void {
      trace("CnGame.setEntry. Warning: This method should be overridden. You need not call the base method.");
    }
    
    //==================================================================
    // showContentDisplay
    //==================================================================
    public function showContentDisplay():void {
      if (mo_gameView) {
        mo_gameView.showContentDisplay();
      }
    }
    
    //==================================================================
    // showResultsDisplay
    //==================================================================
    public function showResultsDisplay(ao_winningPlayers:Array):void {
      if (!mo_gameView) {
        return;
      }
      
      mo_gameView.hideWaitingDisplay();
      
      var s_header:String = ResultsHeaders.THANKS;
      var b_showNonWinnerHeader:Boolean = true;
      
      if (ao_winningPlayers && ao_winningPlayers.length > 1) {
        s_header = ResultsHeaders.WINNERS;
        b_showNonWinnerHeader = false;
      }
      
      mo_gameView.showResultsDisplay(s_header, ao_winningPlayers, b_showNonWinnerHeader);
    }
    
    //==================================================================
    // showWaitingDisplay
    //==================================================================
    public function showWaitingDisplay(u_waitingMode:uint):void {
      if (mo_gameView) {
        mo_gameView.showWaitingDisplay(u_waitingMode);
        gameBgElementsVisible = false;
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      uninitGameView();
      mo_engine = null;
    }
    
    //==================================================================
    // updateEntry
    //==================================================================
    public function updateEntry():void {
      var o_progress:PlayerGameProgress = getPlayerGameProgress();
      if (o_progress) {
        setEntry(o_progress.round);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addGameView
    //==================================================================
    protected function addGameView(o_gameView:CnBaseGameView, b_visible:Boolean):void {
      mo_gameView = o_gameView;
      
      if (mo_gameView) {
        mo_gameView.visible = b_visible;
        var o_mainViewDisplay:Sprite = getEngine().mainView.display;
        o_mainViewDisplay.addChildAt(mo_gameView.display, 0);
      }
    }
    
    //==================================================================
    // getEntryAt
    //==================================================================
    protected function getEntryAt(u_index:uint):Object {
      var ao_entries:Array = getEntries();
      return(ao_entries[u_index]);
    }
    
    //==================================================================
    // getEntries
    //==================================================================
    protected function getEntries():Array {
      var o_engine:I_ClientEngine = getEngine();
      var ao_entries:Array = [];
      if (o_engine) {
        var o_gameSettings:Object = o_engine.serverState.getObject(ServerStateKeys.ACTIVE_GAME_SETTINGS);
        ao_entries = o_gameSettings.entries as Array || ao_entries;
      }
      return(ao_entries);
    }
    
    //==================================================================
    // getPlayerGameProgress
    //==================================================================
    protected function getPlayerGameProgress():PlayerGameProgress {
      var o_engine:I_ClientEngine = getEngine();
      return(o_engine ?
        o_engine.thisPlayerData.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress : null);
    }
    
    //==================================================================
    // handleLastRoundComplete
    //==================================================================
    protected function handleLastRoundComplete():void {
      mo_gameView.display.stage.focus = null;
      mo_gameView.nextButtonsEnabled = false;
      mo_gameView.enabled = false;
    }
    
    //==================================================================
    // handleLastRoundBegin
    //==================================================================
    protected function handleLastRoundBegin():void {
      mo_gameView.showFinishButton();
    }
    
    //==================================================================
    // onGetRoundResponse
    //==================================================================
    protected function onGetRoundResponse():Object {
      return(mo_gameView ? mo_gameView.response : null);
    }
    
    //==================================================================
    // setNextEntry
    //==================================================================
    protected function setNextEntry():void {
      var o_progress:PlayerGameProgress = getPlayerGameProgress();
      if (!o_progress) {
        return;
      }
      
      var u_nextRound:uint = o_progress.round + 1;
      var u_maxRounds:uint = getEntries().length;
      if (u_nextRound == u_maxRounds - 1) {
        handleLastRoundBegin();
      } else if (u_nextRound >= u_maxRounds) {
        handleLastRoundComplete();
        return;
      }
      
      o_progress.round = u_nextRound;
      updateEntry();
    }
    
    //==================================================================
    // uninitGameView
    //==================================================================
    protected function uninitGameView():void {
      if (!mo_gameView) {
        return;
      }
      
      mo_gameView.uninit();
      mo_gameView = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getNumberActiveClientIds
    //==================================================================
    private function getNumberActiveClientIds():uint {
      var as_ids:Array = mo_engine.getActiveClientIds();
      return(as_ids ? as_ids.length : 0);
    }
    
    //==================================================================
    // notifyServerThatClientIsReadyToStartGame
    //==================================================================
    private function notifyServerThatClientIsReadyToStartGame():void {
      if (!mo_engine) {
        return;
      }
      
      var o_message:Object = { };
      o_message[MessageProperties.TYPE] = ClientMessageTypes.READY;
      mo_engine.sendMessageToServer(o_message);
      
      mo_engine.onReadyToStartGame();
    }
    
    //==================================================================
    // notifyThemeImagesComplete
    //==================================================================
    private function notifyThemeImagesComplete():void {
      if (mo_engine) {
        mo_engine.gameOnThemeImagesComplete();
      }
    }
  }
}