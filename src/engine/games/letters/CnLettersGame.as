/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnLettersGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-16-2018
  ActionScript:  3.0
  Description:  
  History:
    07-16-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.games.letters {

	import engine.games.CnGame;
	import engine.I_ClientEngine;
  import engine.ui.lettersView.CnLettersView;
  import flash.display.Sprite;
  import flib.utils.FDataBlock;
	
  public class CnLettersGame extends CnGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_lettersView:CnLettersView;
    private var mo_currentEntry:CnLettersGameEntry;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnLettersGame(o_engine:I_ClientEngine, s_gameId:String) {
      super(o_engine, s_gameId);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // setEntry
    //==================================================================
    override public function setEntry(u_index:uint):void {
      var o_engine:I_ClientEngine = getEngine();
      if (!o_engine) {
        trace("CnLettersGame.setEntry. Warning: Client engine not set.");
        return;
      }
      
      var ao_entries:Array = getEntries();
      var o_entry:Object = ao_entries[u_index];
      if (o_entry == null) {
        mo_currentEntry = null;
        mo_lettersView.visible = false;
        return;
      }
      
      mo_currentEntry = new CnLettersGameEntry(o_entry, u_index);
      mo_lettersView.setEntry(mo_currentEntry.index + 1, mo_currentEntry.word);
      mo_lettersView.visible = true;
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_lettersView = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initGameDisplay();
    }
    
    //==================================================================
    // initGameDisplay
    //==================================================================
    private function initGameDisplay():void {
      mo_lettersView = new CnLettersView(getEngine(), this);
      addGameView(mo_lettersView, false);
    }
  }
}