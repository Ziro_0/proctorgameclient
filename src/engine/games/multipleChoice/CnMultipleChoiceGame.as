/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  CnMultipleChoiceGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-10-2018
  ActionScript:  3.0
  Description:  
  History:
    07-10-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.games.multipleChoice {

  import engine.I_ClientEngine;
  import engine.games.CnGame;
  import engine.ui.multipleChoiceView.CnMultipleChoiceView;
	
  public class CnMultipleChoiceGame extends CnGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_multipleChoiceView:CnMultipleChoiceView;
    private var mo_currentEntry:CnMultipleChoiceGameEntry;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function CnMultipleChoiceGame(o_engine:I_ClientEngine, s_gameId:String) {
      super(o_engine, s_gameId);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // setEntry
    //==================================================================
    override public function setEntry(u_index:uint):void {
      var o_engine:I_ClientEngine = getEngine();
      if (!o_engine) {
        trace("CnMultipleChoiceGame.setEntry. Warning: Client engine not set.");
        return;
      }
      
      var ao_entries:Array = getEntries();
      var o_entry:Object = ao_entries[u_index];
      if (!o_entry) {
        mo_currentEntry = null;
        mo_multipleChoiceView.visible = false;
        return;
      }
      
      mo_currentEntry = new CnMultipleChoiceGameEntry(o_entry, u_index);
      mo_multipleChoiceView.setEntry(mo_currentEntry);
      mo_multipleChoiceView.visible = true;
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_multipleChoiceView = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initMultipleChoiceDisplay();
    }
    
    //==================================================================
    // initMultipleChoiceDisplay
    //==================================================================
    private function initMultipleChoiceDisplay():void {
      mo_multipleChoiceView = new CnMultipleChoiceView(getEngine(), this);
      addGameView(mo_multipleChoiceView, false);
    }
    
    //==================================================================
    // setCorrectOption
    //==================================================================
    private function setCorrectOption():void {
      var i_index:int = getCorrectChoice();
      mo_multipleChoiceView.selectCorrectOption(i_index);
    }
    
    //==================================================================
    // getCorrectChoice
    //==================================================================
    private function getCorrectChoice():int {
      for (var i_index:int = 0; i_index < mo_currentEntry.choices.length; i_index++) {
        var o_choice:CnMultipleChoiceGameChoice = mo_currentEntry.choices[i_index];
        if (o_choice.isCorrect) {
          return(i_index);
        }
      }
      
      return( -1);
    }
  }
}