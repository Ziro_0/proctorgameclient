/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ProctorGameClientApp

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	Document class for game client engine.
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package {
  CONFIG::IS_ANDROID {
    import com.freshplanet.ane.KeyboardSize.MeasureKeyboard;
  }
  
  import engine.ClientEngine;
  import engine.common.core.App;
  import engine.common.core.Engine;
  import engine.common.core.EngineInitData;
  import engine.common.data.ClientSharedData;
  import engine.common.data.CustomThemePath;
  import engine.common.ui.introView.IntroView;
  import engine.data.CnDebugData;
  import engine.data.CnDesignData;
  import engine.ui.mainView.CnMainView;
	
  public class ProctorGameClientApp extends App {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:Engine;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ProctorGameClientApp():void {
      initDeviceProfile();
      
      var o_engineInitData:EngineInitData = new EngineInitData();
      o_engineInitData.app = this;
      o_engineInitData.engineClass = ClientEngine;
      o_engineInitData.introViewClass = IntroView;
      o_engineInitData.mainViewClass = CnMainView;
      o_engineInitData.sharedDataClass = ClientSharedData;
      o_engineInitData.versionText = version;
      o_engineInitData.ipAddress = CnDebugData.IP_ADDRESS;
      o_engineInitData.themesBaseUrl = CnDesignData.THEMES_BASE_URL;
      o_engineInitData.customThemesBaseUrl = CustomThemePath.CUSTOM_BASE_PATH;
      super(o_engineInitData);
      
      CONFIG::IS_ANDROID {
        initMeasureKeyboard();
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // initMeasureKeyboard
    //==================================================================
    CONFIG::IS_ANDROID {
      private function initMeasureKeyboard():void {
        new MeasureKeyboard();
      }
    }
  }
}