:: Set working dir
cd %~dp0 & cd ..

:user_configuration

:: About AIR application packaging
:: http://help.adobe.com/en_US/air/build/WS5b3ccc516d4fbf351e63e3d118666ade46-7fd9.html

:: NOTICE: all paths are relative to project root

:: Android packaging
set AND_CERT_NAME=%LS_AND_CERT_NAME%
set AND_CERT_PASS=%LS_AND_CERT_PASS%
set AND_CERT_FILE=%LS_AND_CERT_FILE%
set AND_ICONS=icons/android

set AND_SIGNING_OPTIONS=-storetype pkcs12 -keystore "%AND_CERT_FILE%" -storepass %AND_CERT_PASS%

set IOS_DIST_CERT_FILE=%LS_IOS_MOBILE_APP_STORE_CERT_FILE%
set IOS_DIST_CERT_PASS=%LS_IOS_MOBILE_APP_STORE_CERT_PASS%
set IOS_DIST_PROVISION=%LS_IOS_MOBILE_APP_STORE_PROVISION%

set IOS_DIST_SIGNING_OPTIONS=-storetype pkcs12 -keystore "%IOS_DIST_CERT_FILE%" -provisioning-profile %IOS_DIST_PROVISION% -storepass %IOS_DIST_CERT_PASS%

set IOS_ICONS=icons/ios

:: Application descriptor
set APP_XML=applicationMobileAppStore.xml

:: Files to package
set APP_DIR=bin
set FILE_OR_DIR=-C %APP_DIR% .

:: Your application ID (must match <id> of Application descriptor) and remove spaces
for /f "tokens=3 delims=<>" %%a in ('findstr /R /C:"^[ 	]*<id>" %APP_XML%') do set APP_ID=%%a
set APP_ID=%APP_ID: =%

:: Output packages
set DIST_PATH=dist
set DIST_NAME=ProctorGameClient

:: Debugging using a custom IP
set DEBUG_IP=%LS_DEBUG_IP%

:validation
findstr /C:"<id>%APP_ID%</id>" "%APP_XML%" > NUL
if errorlevel 1 goto badid
goto end

:badid
echo.
echo ERROR: 
echo   Application ID in 'bat\SetupApp_MobileAppStore.bat' (APP_ID) 
echo   does NOT match Application descriptor '%APP_XML%' (id)
echo.

:end
