@echo off

:: Set working dir
cd %~dp0 & cd ..

call bat\local-settings.bat
call bat\SetupSDK.bat

echo --- iOS ---
call adt -devices -platform ios

echo --- Android ---
call adt -devices -platform android
