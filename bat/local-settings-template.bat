:: Copy this file to "local-settings.bat" and fill in with valid local settings

:: Static path to Flex SDK
set LS_FLEX_SDK=

:: Path to Android SDK
set LS_ANDROID_SDK=

:: Android packaging
set LS_AND_CERT_NAME=""
set LS_AND_CERT_PASS=
set LS_AND_CERT_FILE=

:: iOS packaging
set LS_IOS_DEV_CERT_FILE=
set LS_IOS_DEV_CERT_PASS=
set LS_IOS_DEV_PROVISION=

set LS_AUTO_INSTALL_IOS="yes"

set LS_IOS_DIST_CERT_FILE=
set LS_IOS_DIST_CERT_PASS=
set LS_IOS_DIST_PROVISION=

set LS_IOS_APP_STORE_CERT_FILE=
set LS_IOS_APP_STORE_CERT_PASS=
set LS_IOS_APP_STORE_PROVISION=

set LS_IOS_MOBILE_APP_STORE_CERT_FILE=
set LS_IOS_MOBILE_APP_STORE_CERT_PASS=
set LS_IOS_MOBILE_APP_STORE_PROVISION=

:: Screen dimensions only used with RunApp for desktop
set LS_SCREEN_WIDTH=
set LS_SCREEN_HEIGHT=

:: Debugging using a custom IP
set LS_DEBUG_IP=